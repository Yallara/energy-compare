FROM eclipse-temurin:17-jre-alpine
MAINTAINER Yallara <yallara@yallara.au>

RUN mkdir -p /opt/data
ADD EnergyCompare*.tbz2 /opt
RUN ln -s /opt/EnergyCompare* /opt/prog && \
    echo "/opt/data/" > /opt/prog/bin/docker.conf

VOLUME ["/opt/data"]

ENTRYPOINT ["/opt/prog/bin/EnergyCompare"]

CMD ["--help"]
