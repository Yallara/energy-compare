/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import picocli.CommandLine

/**
 * Interfaces with picocli CLI library to provide version information to the user.
 */
class VersionProvider implements CommandLine.IVersionProvider {

    @Override
    String[] getVersion() throws Exception {
        Map<String, String> vsnInfo = EnergyCompare.versionInfoMap()

        return vsnInfo ?
               [
                 "Version ${vsnInfo.version}",
                 "SCM ${vsnInfo.scmRevision}",
                 "Build date ${vsnInfo.buildDate}",
               ] :
               ['Version Unknown']
    }
}
