/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import groovy.transform.CompileStatic
import groovy.transform.Memoized
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.zone.ZoneOffsetTransition
import java.time.zone.ZoneOffsetTransitionRule
import java.time.zone.ZoneRules

/**
 * Supports conversion of dates and times between standard (offset from UTC) time and the timezone, which
 * may include daylight savings time.
 */
@CompileStatic
class TimezoneSupport {

    String offsetName
    String zoneName

    private ZoneId zoneZoneId
    private ZoneId offsetZoneId
    private ZoneRules zoneRules
    private ZoneRules offsetRules
    private final List<LocalDate> transitionDates = []

    /**
     * Constructor.
     * <p>
     *     Defaults to Victoria if null values are passed.
     *     <p>
     *         See the TZ Identifier at https://en.wikipedia.org/wiki/List_of_tz_database_time_zones for valid zoneNames
     *
     * @param offset the offset for local standard time. If null, defaults to 'UTC+10:00'
     * @param zoneName the standard name of the time zone. If null, defaults to 'Australia/Melbourne'.
     */
    TimezoneSupport(@Nullable String offset, @Nullable String zoneName) {
        this.offsetName = offset ?: 'UTC+10:00'
        this.zoneName = zoneName ?: 'Australia/Melbourne'
    }

    /**
     * Returns the Java ZoneId for this timezone.
     *
     * @return the ZoneId object representing, e.g. Australia/Melbourne
     */
    @NotNull
    protected ZoneId getZoneZoneId() {
        if (!zoneZoneId) {
            zoneZoneId = ZoneId.of(zoneName)
        }
        return zoneZoneId
    }

    /**
     * Returns the Java ZoneId for this time offset.
     *
     * @return the ZoneId object representing, e.g. UTC+10:00
     */
    @NotNull
    protected ZoneId getOffsetZoneId() {
        if (!offsetZoneId) {
            offsetZoneId = ZoneId.of(offsetName)
        }
        return offsetZoneId
    }

    /**
     * Return the ZoneRules for the timezone (e.g. Australia/Melbourne)
     *
     * @return the ZoneRules object
     */
    @NotNull
    protected ZoneRules getZoneRules() {
        if (!zoneRules) {
            zoneRules = getZoneZoneId().rules
        }
        return zoneRules
    }

    /**
     * Returns the ZoneRules for the offset (e.g. UTC+10:00)
     *
     * @return the ZoneRules object
     */
    @NotNull
    protected ZoneRules getOffsetRules() {
        if (!offsetRules) {
            offsetRules = getOffsetZoneId().rules
        }
        return offsetRules
    }

    /**
     * Is the passed date a day where there was a transition to or from daylight savings time?
     *
     * @param date the date of interest
     * @return whether the passed date was a transition to/from daylight savings
     */
    @Memoized
    Boolean isDstTransition(@NotNull LocalDate date) {
        if (transitionDates.isEmpty()) {
            buildTransitionDates()
        }
        return transitionDates.contains(date)
    }

    /**
     * Convert the passed LocalDate and time from an offset-based time to a timezone-based time.
     * <p>
     *     Attempts to convert the passed object to a LocalTime object.
     *
     * @param date the date to convert
     * @param object the time on that date
     * @return a LocalTime representing the passed date and time, based on the timezone.
     * @throws NumberFormatException if the passed time object is null or cannot be converted to a LocalTime.
     */
    @NotNull
    LocalTime offsetToZone(@NotNull LocalDate date, @Nullable Object object) throws NumberFormatException {
        LocalTime time = Util.asLocalTime(object)
        LocalDateTime converted = offsetToZone(LocalDateTime.of(date, time))
        return converted.toLocalTime()
    }

    /**
     * Convert the passed LocalDate and time from a timezone-based time to an offset-based time.
     * <p>
     *     Attempts to convert the passed object to a LocalTime object.
     *
     * @param date the date to convert
     * @param object the time on that date
     * @return a LocalTime representing the passed date and time, based on the offset.
     * @throws NumberFormatException if the passed time object is null or cannot be converted to a LocalTime.
     */
    @NotNull
    LocalTime zoneToOffset(@NotNull LocalDate date, @Nullable Object object) throws NumberFormatException {
        LocalTime time = Util.asLocalTime(object)
        LocalDateTime converted = zoneToOffset(LocalDateTime.of(date, time))
        return converted.toLocalTime()
    }

    /**
     * Convert the passed LocalDateTime, assumed to represent a time with an offset, to the corresponding timezone LocalDateTime.
     *
     * @param from the LocalDateTime to convert
     * @return the converted LocalDateTime
     */
    @NotNull
    LocalDateTime offsetToZone(@NotNull LocalDateTime from) {
        // straightforward conversion, because zone time not being used in db
        ZonedDateTime standardTime = ZonedDateTime.of(from, getOffsetZoneId())
        ZonedDateTime zoneTime = standardTime.withZoneSameInstant(getZoneZoneId())

        return zoneTime.toLocalDateTime()
    }

    /**
     * Convert the passed LocalDateTime, assumed to represent a time in a timezone, to the corresponding offset-based LocalDateTime.
     *
     * @param from the LocalDateTime to convert
     * @return the converted LocalDateTime
     */
    @NotNull
    LocalDateTime zoneToOffset(@NotNull LocalDateTime from) {
        ZonedDateTime zoneTime = ZonedDateTime.of(from, getZoneZoneId())
        ZonedDateTime offsetTime = zoneTime.withZoneSameInstant(getOffsetZoneId())
        return offsetTime.toLocalDateTime()
    }

    /**
     * Convert the passed LocalTime on the passed date from timezone-based time to an offset-based time.
     *
     * @param dt the date
     * @param tm the time
     * @return the converted LocalTime
     */
    @NotNull
    LocalTime zoneTimeToOffset(@NotNull LocalDate dt, @NotNull LocalTime tm) {
        LocalDateTime dateTime = zoneToOffset(LocalDateTime.of(dt, tm))
        return dateTime.toLocalTime()
    }

    /**
     * Convert the passed LocalTime on the passed date from offset-based time to an timezone-based time.
     *
     * @param dt the date
     * @param tm the time
     * @return the converted LocalTime
     */
    @NotNull LocalTime offsetTimeToZone(@NotNull LocalDate dt, @NotNull LocalTime tm) {
        LocalDateTime dateTime = offsetToZone(LocalDateTime.of(dt, tm))
        return dateTime.toLocalTime()
    }

    /**
     * Generate a list of dates on which daylight savings either started or ended, for every
     * year starting from 2020 to current.
     *
     * Used internally.
     */
    protected void buildTransitionDates() {
        List<ZoneOffsetTransitionRule> transRules = getZoneRules().getTransitionRules()
        LocalDate now = LocalDate.now()
        Integer currentYear = now.year
        (2020..currentYear).each { year ->
            transRules.each { rule ->
                ZoneOffsetTransition trans = rule.createTransition(year)
                transitionDates << trans.dateTimeBefore.toLocalDate()
            }
        }
    }
}
