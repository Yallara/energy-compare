/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.exception.DbException
import au.yallara.energy.exception.UnsupportedFormatException
import groovy.util.logging.Slf4j
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters

import java.time.LocalDate
import java.util.concurrent.Callable

/**
 * Main entry point class.
 *
 * Manages commandline options, drives execution based on those options.
 */
@Slf4j
@Command(name = 'EnergyCompare', description = 'Compare cost of electricity from different providers',
  versionProvider = VersionProvider.class)
class EnergyCompare implements Callable<Integer> {

    @Parameters(index = '0', description = 'Name of meter to use')
    String meterName

    @Option(names = ['-p', '--plan'], description = 'File containing details of plans to compare')
    String planFile

    @Option(names = ['-b', '--breakdown'], description = 'Print breakdown of costs - use with --plan')
    Boolean breakdown

    @Option(names = ['-d', '--data'],
      description = 'CSV file from power supplier containing interval meter data, either AEMO MDFF NEM12 or AGL format')
    String dataFile

    @Option(names = ['-f', '--folder'],
      description = 'Path to location of databases used by this program. Defaults to current directory.')
    String dbPath

    @Option(names = ['-o', '--out'],
      description = 'File to write results to. Defaults to STDOUT.')
    String outFile

    @Option(names = ['-s', '--start'],
      description = 'Start date of data to use (yyyy-MM-dd). Defaults to 1 year before end date (see --end), or oldest data if 1 year before not available.')
    String startDate

    @Option(names = ['-e', '--end'],
      description = 'End date to use for comparison (yyyy-MM-dd). Defaults to most recent date available. See also --start.')
    String endDate

    @Option(names = ['-h', '--help'], description = 'Print usage information and exit', usageHelp = true)
    Boolean help

    @Option(names = ['-v', '--version'], description = 'Display version information and exit.', versionHelp = true)
    Boolean version

    @Option(names = ['-z', '--zone'], description = 'Time zone as String. Defaults to "Australia/Melbourne".')
    String timeZoneName = 'Australia/Melbourne'

    @Option(names = ['-t', '--timeoffset'], description = 'Time offset for local *standard time*. Defaults to "UTC+10:00" (Eastern Australia).')
    String timeOffset = 'UTC+10:00'

    static void main(String[] args) {
        Integer exitCode = new CommandLine(new EnergyCompare()).execute(args)
        System.exit(exitCode)
    }

    @Override
    Integer call() throws Exception {
        File dockerconf = new File('/opt/prog/bin/docker.conf')
        Util.baseDir = dockerconf.canRead() ? dockerconf.readLines().first() : './'

        PrintWriter output = outFile ? new PrintWriter(Util.filePath(outFile)) : new PrintWriter(System.out)
        String dbLocation = dbPath ?: '.'

        try {
            TimezoneSupport timezoneSupport = new TimezoneSupport(timeOffset, timeZoneName)
            DataManager dbManager = new DataManager(dbLocation, meterName)
            if (!dataFile && !planFile) {
                // simply output some stats from previously imported data, or explain that's not possible
                new StatsMaker(dbManager).calculate(output)
                return 0
            }

            if (dataFile) {
                try {
                    Reader reader = new FileReader(Util.filePath(dataFile))
                    dbManager.importData(reader, timezoneSupport)
                } catch (IllegalArgumentException iae) {
                    output.println("Import file has unsupported format: ${iae.message}")
                    log.error("Import file format unsupported $dataFile", iae)
                }
            }
            if (planFile) {
                PlanComparator comparator = new PlanComparator(dbManager, planFile, timezoneSupport)
                LocalDate start = startDate ? LocalDate.parse(startDate) : null
                LocalDate end = endDate ? LocalDate.parse(endDate) : null
                if (breakdown) {
                    Map<String, CostBreakdown> breakdownList = comparator.calcPlanBreakdowns(start, end)
                    printBreakdowns(output, breakdownList, comparator.actualStart, comparator.actualEnd)
                } else {
                    Map<String, BigDecimal> costs = comparator.calcPlanTotals(start, end)
                    printCosts(output, costs, comparator.actualStart, comparator.actualEnd)
                }
            }

            return 0
        } catch (DbException dbe) {
            log.warn('Database problems', dbe)
            output.println("Failed to work with database: ${dbe.message}")
            return 2
        } catch ( UnsupportedFormatException usfe) {
            log.warn('Data file format not supported', usfe)
            output.println("Unsupported data file format: ${usfe.message}")
        } finally {
            output.close()
        }
    }

    static Map<String, String> versionInfoMap() {
        InputStream is = null
        Map<String, String> result = [:]

        try {
            is = EnergyCompare.getClass().getResourceAsStream('/version.properties')
            if (is) {
                Properties p = new Properties()
                p.load(is)
                result.version = p.version
                result.scmRevision = p.scmRevision
                result.buildDate = p.buildDate
            }
        } finally {
            is?.close()
        }

        return result
    }

    @SuppressWarnings('unused')
    static void versionInfo(PrintStream sout = null, Boolean incExtra = false) {
        String vsn = 'Unknown'
        String scmRev = ''
        String buildDate = ''
        PrintStream pout = sout ?: System.out

        Map<String, String> vsnInfo = versionInfoMap()

        if (vsnInfo) {
            pout.println("Energy Compare Version $vsn")
            if (incExtra) {
                pout.println("SCM Revision: $scmRev")
                pout.println("Build Date: $buildDate")
            }
        } else {
            pout.println('Energy Compare Version Unknown')
        }
    }

    protected void printCosts(PrintWriter output, Map<String, BigDecimal> costMap, LocalDate start, LocalDate end) {
        output.println("Calculated cost per plan based on data available between $start and $end")
        String bestPlan = costMap.min { it.value }.key
        costMap.sort { it.value }.each { planName, cost ->
            String marker = planName == bestPlan ? '**' : ''
            output.println("$marker$planName : ${Util.asCurrencyString(cost)}")
        }
    }

    void printBreakdowns(PrintWriter output, Map<String, CostBreakdown> costBreakdownMap, LocalDate actualStart, LocalDate actualEnd) {
        output.println("Cost breakdown per plan based on data available between $actualStart and $actualEnd")
        String bestPlan = costBreakdownMap.min { it.value.total }.key
        costBreakdownMap.sort { it.value.total }.each { planName, costBreakdown ->
            output.println('-----')
            if (planName == bestPlan) {
                output.println('**********')
            }
            output.println(planName)
            output.println(costBreakdown)
            output.println('-----')
        }
    }
}
