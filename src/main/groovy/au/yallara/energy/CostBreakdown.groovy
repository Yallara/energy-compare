/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.tariff.Tariff
import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalTime

/**
 * Calculates the breakdown of the costs of electricity.
 *
 * Provided with kWh from meter data, and an Energy Plan,
 * calculates the costs associated with each tariff type,
 * as well as the overall total.
 *
 */
@CompileStatic
class CostBreakdown {
    final LocalDate startDate
    final LocalDate endDate
    final EnergyPlanDetails planDetails
    private Integer days = 0

    private BigDecimal totalCost = null

    /**
     * Constructor for the calculations related to a single energy plan.
     *
     * 'start' and 'end' LocalDates are used only by toString() to
     * provide context to the result.
     *
     * @param plan the energy plan containing Tariffs and fees
     * @param start the starting date for the data
     * @param end the ending date for the data
     */
    CostBreakdown(@NotNull EnergyPlanDetails plan, @NotNull LocalDate start, @NotNull LocalDate end) {
        startDate = start
        endDate = end
        planDetails = plan
    }

    /**
     * The number of days of data.
     *
     * This is usually set after all kWh values have been added. It may
     * be different to the difference between start and end dates.
     *
     * @param num number of days with data.
     */
    void setDays(@NotNull Integer num) {
        days = num
    }

    /**
     * Calculates the total supply charge, in dollars.
     *
     * @return the calculated total supply charge
     */
    @NotNull
    BigDecimal getSupplyCharge() {
        return days * planDetails.supplyDailyCost
    }

    /**
     * Calculates the 'cost' (or return) for energy exported to the grid.
     *
     * @return the value of the energy exported to the grid, in dollars
     */
    @NotNull
    BigDecimal getExportCost() {
        return planDetails.exportTariffs.isEmpty() ? BigDecimal.ZERO : planDetails.exportTariffs.sum { it.calculate(days) } as BigDecimal
    }

    /**
     * Calculates the cost of imported energy.
     *
     * @return the total cost of imported energy
     */
    @NotNull
    BigDecimal getImportCost() {
        return planDetails.importTariffs.sum { it.calculate(days) } as BigDecimal
    }

    /**
     * Calculates the total amount of energy exported, in kWh.
     *
     * @return the total energy in kWh
     */
    @NotNull
    BigDecimal getExportKwh() {
        return planDetails.exportTariffs.isEmpty() ? BigDecimal.ZERO : planDetails.exportTariffs.sum { it.getKwh() } as BigDecimal
    }

    /**
     * Calculates the total amount of energy imported, in kWh.
     *
     * @return the total energy in kWh
     */
    @NotNull
    BigDecimal getImportKwh() {
        return planDetails.importTariffs.sum { it.getKwh() } as BigDecimal
    }

    /**
     * Adds the specified import or export energy, in kWh, to the appropriate tariff.
     * <p>
     * startTime is normally the time provided in the interval meter data. It is the starting time
     * of the associated interval of energy use, and is the time used to determine which tariff applies.
     * <p>
     *     Times should be in standard time, not summer or similar time. Any necessary adjustments will
     *     be made internally.
     *
     * @param direction which direction was the energy? EnergyDirection.In for import, EnergyDirection.OUT for export.
     * @param startTime the time this energy transfer occurred
     * @param kwh the amount of energy transferred
     */
    void addKwh(@NotNull EnergyDirection direction, @NotNull LocalDate date, @NotNull LocalTime startTime, @NotNull BigDecimal kwh) {

        List<Tariff> tariffList = direction == EnergyDirection.OUT ? planDetails.exportTariffs : planDetails.importTariffs

        if (tariffList && !tariffList.isEmpty()) {
            Tariff relevantTariff = tariffList.find { it.applies(date, startTime) }
            relevantTariff.addKwh(kwh)
        }
    }

    /**
     * Calculates the total cost of the energy under this plan, in dollars.
     * <p>
     * IMPORTANT: do not call this method until all the import and export kWh values have been added, and the
     * number of days set. For efficiency, this value is calculated the first time the method is called,
     * then saved and the saved value is returned for subsequent calls. Which means any changes to kWh or number of days
     * after the first use of this method will not include those changes in the total.
     * <p>
     * This method assumes exported energy income under an export tariff is subtracted from
     * the costs of supply and import.
     *
     * @return the total cost under this plan, in dollars. May be negative.
     */
    @NotNull
    BigDecimal getTotal() {
        // this gets called a few times, so save result
        // this could create a problem if kwh etc are altered after this is called the first time, but in normal use that won't happen.
        if (totalCost == null) {
            totalCost = getSupplyCharge() + getImportCost() - getExportCost()
        }
        return totalCost
    }

    /**
     * Get the total kWh a specific tariff applies to.
     *
     * @param tariffName the name of the tariff of interest
     * @return the number of kWh transferred under this tariff
     * @throws IllegalArgumentException if the tariff name is not recognised
     */
    @NotNull
    BigDecimal getTariffKwh(@NotNull String tariffName) throws IllegalArgumentException {
        Tariff tariff = findTariff(tariffName)
        return tariff.kwh
    }

    /**
     * Get the total cost under a specific tariff.
     *
     * @param tariffName the name of the tariff of interest
     * @return the total cost under this tariff
     * @throws IllegalArgumentException if the tariff name is not recognised
     */
    @NotNull
    BigDecimal getTariffCost(@NotNull String tariffName) throws IllegalArgumentException {
        Tariff tariff = findTariff(tariffName)
        return tariff.calculate(days)
    }

    /**
     * Find a tariff with a specific name.
     * <p>
     *     Checks import tariffs, then export tariffs.
     *
     * @param tariffName the name of the tariff of interest
     * @return the tariff with the specified name
     * @throws IllegalArgumentException if the named tariff could not be found
     */
    @NotNull
    protected Tariff findTariff(@NotNull String tariffName) throws IllegalArgumentException {
        Tariff tariff = planDetails.importTariffs.find { it.tariffName == tariffName }
        if (!tariff) {
            tariff = planDetails.exportTariffs.find { it.tariffName == tariffName }
        }
        if (!tariff) {
            throw new IllegalArgumentException("Tariff named $tariffName not found in plan ${planDetails.planName}")
        }
        return tariff

    }

    /**
     * A human-friendly summary of the results.
     *
     * @return a String with a summary of the results in human-readable form
     */
    @Override
    String toString() {
        String supplyStr = Util.asCurrencyString(getSupplyCharge())

        List<String> importTariffStrs = planDetails.importTariffs.collect { tariff ->
            String cost = Util.asCurrencyString(tariff.calculate(days))
            String usage = Util.asKwhString(tariff.getKwh())
            "${tariff.tariffName}: $cost ($usage kWh)".toString()
        }
        List<String> exportTariffStrs = planDetails.exportTariffs.collect { tariff ->
            String cost = Util.asCurrencyString(tariff.calculate(days))
            String usage = Util.asKwhString(tariff.getKwh())
            "${tariff.tariffName}: $cost ($usage kWh)".toString()
        }
        String totalImportStr = "${Util.asCurrencyString(getImportCost())} (${Util.asKwhString(getImportKwh())} kWh)"
        String totalExportStr = "${Util.asCurrencyString(getExportCost())} (${Util.asKwhString(getExportKwh())} kWh)"

        String totalString = Util.asCurrencyString(getTotal())

        return """
${startDate} - ${endDate} (${days})
Supply: ${supplyStr}
Import:
    ${importTariffStrs.join('\n    ')}
  Total Import: $totalImportStr
Export:
    ${exportTariffStrs.join('\n    ')}
  Total Export: $totalExportStr
TOTAL: ${totalString}
"""
    }

    /**
     * Determine equality.
     *
     * Uses start and end dates, and name of plan.
     *
     * @param o Object to compare with
     * @return whether two objects are considered equal
     */
    @Override
    boolean equals(@Nullable o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof CostBreakdown)) {
            return false
        }

        CostBreakdown that = (CostBreakdown) o

        if (endDate != that.endDate
          || planDetails.planName != that.planDetails.planName
          || startDate != that.startDate) {
            return false
        }

        return true
    }

    /**
     * Calculate hash for this object, using name of plan plus start and end dates.
     *
     * @return the calculated hashcode.
     */
    @Override
    int hashCode() {
        int result
        result = (startDate != null ? startDate.hashCode() : 0)
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0)
        result = 31 * result + (planDetails.planName != null ? planDetails.planName.hashCode() : 0)
        return result
    }
}
