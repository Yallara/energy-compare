/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.exception.DbException
import au.yallara.energy.exception.PlanFormatException
import au.yallara.energy.tariff.SimpleTariff
import au.yallara.energy.tariff.SteppedTariff
import au.yallara.energy.tariff.Tariff
import au.yallara.energy.tariff.TouTariff
import groovy.util.logging.Slf4j
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.Month

/**
 * Loads energy plans from a config file, and manages comparisons between them.
 *
 */
@Slf4j
class PlanComparator {
    private final DataManager dataManager
    private final List<EnergyPlanDetails> plans
    private static final LocalTime dayStart = LocalTime.of(0, 0, 0)
    private static final LocalTime dayEnd = LocalTime.of(23, 59, 59)
    private final TimezoneSupport timezoneSupport

    LocalDate actualStart
    LocalDate actualEnd

    /**
     * Constructor, loads energy plans from passed config file.
     *
     * See the README for information about the config file format.
     *
     * @param manager the DataManager to use when interacting with the database.
     * @param planFilename the path and filename of the file containing the energy plan specifications.
     * @param timezoneSupport a TimezoneSupport object initialised with the local Timezone and UTC offset
     */
    PlanComparator(@NotNull DataManager manager, @NotNull String planFilename, @NotNull TimezoneSupport timezoneSupport) {
        dataManager = manager
        plans = loadEnergyPlans(planFilename)
        this.timezoneSupport = timezoneSupport
    }

    /**
     * Constructor, for use by test code.
     *
     * Does not load any plans, allowing the test code to test the loading.
     *
     * @param manager the DataManager to use when interacting with the database - often a Stub when testing.
     * @param timezoneSupport a TimezoneSupport object initialised with the local Timezone and UTC offset
     */
    PlanComparator(@NotNull DataManager manager, @NotNull TimezoneSupport timezoneSupport) {
        dataManager = manager
        plans = []
        this.timezoneSupport = timezoneSupport
    }

    /**
     * Return a List of the loaded energy plans.
     * @return
     */
    @Nullable
    List<EnergyPlanDetails> getPlans() {
        return plans
    }

    /**
     * Loads energy plans from a configuration file.
     *
     * @param planFilename path and name of plan configuration file
     * @return a List of the loaded plans
     * @throws FileNotFoundException if the specified file could not be found and read
     * @throws NumberFormatException if expected numbers in the plan file were not actually numbers
     * @throws PlanFormatException if an error is found in the config file - the Exception will include a description of the problem(s)
     */
    @Nullable
    protected List<EnergyPlanDetails> loadEnergyPlans(@NotNull String planFilename)
      throws FileNotFoundException, NumberFormatException, PlanFormatException {
        File planFile = new File(Util.filePath(planFilename))
        if (!planFile.canRead()) {
            throw new FileNotFoundException("Can not read plans file ${planFile.absolutePath}")
        }
        ConfigObject plansConf = new ConfigSlurper().parse(planFile.toURI().toURL())

        return energyPlanConfToData(plansConf)
    }

    /**
     * Loads energy plans from the passed ConfigObject.
     *
     * @param plansMapConf a ConfigObject containing the energy plans details
     * @return a List of the loaded plans
     * @throws NumberFormatException if expected numbers in the plan file were not actually numbers
     * @throws PlanFormatException if an error is found in the config file - the Exception will include a description of the problem(s)
     */
    @Nullable
    protected List<EnergyPlanDetails> energyPlanConfToData(@NotNull ConfigObject plansMapConf) throws NumberFormatException, PlanFormatException {
        List<EnergyPlanDetails> planList = plansMapConf.entrySet().inject([]) { lst, Map.Entry entry ->
            Map<String, Object> planConf = entry.value
            String planName = entry.key
            EnergyPlanDetails energyPlanDetails = new EnergyPlanDetails([
              planName       : planName,
              supplyDailyCost: Util.asBigDecimal(planConf.supplyCost),
            ])
            energyPlanDetails.importTariffs = buildTariffList(planConf.importTariffs as List<Map>, planName)
            energyPlanDetails.exportTariffs = planConf.exportTariffs ? buildTariffList(planConf.exportTariffs as List<Map>, 'ExportTariff') : []

            lst << energyPlanDetails
            lst
        }
        List<String> errors = checkPlansValid(planList)
        if (!errors.isEmpty()) {
            throw new PlanFormatException(errors)
        }
        return planList
    }

    /**
     * Given a List of Maps describing one or more tariffs (normally from within a ConfigObject),
     * generates a List of Tariff objects initialised from that data.
     *
     * @param tariffList the Tariff information, normally as read from a plan file
     * @param name a name to use for this tariff if it does not include one - often the plan name
     * @return a List of Tariff object
     * @throws IllegalArgumentException if the passed data is not in the expected format
     */
    @NotNull
    protected List<Tariff> buildTariffList(@Nullable List<Map> tariffList, @Nullable String name) throws IllegalArgumentException {
        if (tariffList) {
            if (tariffList.size() == 1) {
                // a single tariff
                def tariffData = tariffList[0]
                String tariffName = tariffData.name ?: name
                SimpleTariff tariff = new SimpleTariff(tariffName)
                tariff.setCostPerKwh(Util.asBigDecimal(tariffData.tariff))
                return [tariff]
            } else if (tariffList[0].min || tariffList[0].max) {
                // a SteppedTariff
                SteppedTariff tariff = new SteppedTariff(name)
                tariffList.each { tariffData ->
                    BigDecimal cost = Util.asBigDecimal(tariffData.tariff)
                    BigDecimal min = tariffData.min ? Util.asBigDecimal(tariffData.min) : null
                    BigDecimal max = tariffData.max ? Util.asBigDecimal(tariffData.max) : null
                    tariff.addStep(cost, min, max)
                }
                return [tariff]
            } else {
                // other tariff types (currently only time-of-use TOU)
                return tariffList.inject([]) { lst, tariffData ->
                    LocalTime startTime = tariffData.startTime ? Util.asLocalTime(tariffData.startTime) : dayStart
                    LocalTime endTime = tariffData.endTime ? Util.asLocalTime(tariffData.endTime) : dayEnd
                    BigDecimal tariffCost = Util.asBigDecimal(tariffData.tariff)
                    String tariffName = tariffData.name ?: name

                    Tariff tariff = new TouTariff(tariffName)
                    tariff.costPerKwh = tariffCost
                    tariff.setTimeRange(startTime, endTime)
                    if (tariffData.days) {
                        Collection<DayOfWeek> days = Util.asDaysOfWeek(tariffData.days.toString())
                        tariff.addDaysOfWeek(days)
                    }
                    if (tariffData.months) {
                        Collection<Month> months = Util.asMonths(tariffData.months.toString())
                        tariff.addMonths(months)
                    }
                    lst << tariff
                    lst
                }
            }
        }
        return []
    }

    /**
     * Calculate the total cost of each plan, if it had been in effect between the specified dates.
     * <p>
     * Returns a map where the key is the plan name (read from the configuration file), and the value is the
     * total cost (negative if exported energy was sufficient) in dollars under that plan for the
     * specified date range.
     * <p>
     * The database must contain data for the start and end dates actually used. Therefore the dates will be adjusted if necessary.
     * <p>
     * If the end date is after the last database entry, or null, it will be set to the date of the last database entry.
     * <p>
     * If the start date is null or before the first entry, it will be set to 1 year before the calculated end date. If
     * the start date is still before the date of the first database entry, it will be set to the date of the first
     * database entry.
     *
     * @param startDate preferred starting date for analysis of data - may be null
     * @param endDate preferred end date for analysis of date - may be null
     * @return a Map of calculated costs
     * @throws DbException if there's a problem accessing the database
     */
    @NotNull
    Map<String, BigDecimal> calcPlanTotals(@Nullable LocalDate startDate, @Nullable LocalDate endDate) throws DbException {
        final CostCalculator calculator = new CostCalculator(dataManager, timezoneSupport)
        setActualDates(startDate, endDate)
        return plans.collectEntries { plan ->
            BigDecimal cost = calculator.calculate(plan, actualStart, actualEnd)
            [(plan.planName): cost]
        }
    }

    /**
     * Calculate a breakdown of the costs of each plan, split into supply charge and export and import tariffs.
     * <p>
     * The database must contain data for the start and end dates actually used. Therefore the dates will be adjusted if necessary.
     * <p>
     * If the end date is after the last database entry, or null, it will be set to the date of the last database entry.
     * <p>
     * If the start date is null or before the first entry, it will be set to 1 year before the calculated end date. If
     * the start date is still before the date of the first database entry, it will be set to the date of the first
     * database entry.
     *
     * @param startDate preferred starting date for analysis of data - may be null
     * @param endDate preferred end date for analysis of date - may be null
     * @return a Map with key being the plan name, and the value a CostBreakdown object with the costing details
     * @throws DbException if there's a problem accessing the database
     */
    @NotNull
    Map<String, CostBreakdown> calcPlanBreakdowns(@Nullable LocalDate startDate, @Nullable LocalDate endDate) throws DbException {
        final CostCalculator calculator = new CostCalculator(dataManager, timezoneSupport)
        setActualDates(startDate, endDate)

        return plans.collectEntries { plan ->
            CostBreakdown breakdown = calculator.calculateBreakdown(plan, actualStart, actualEnd)
            [(plan.planName): breakdown]
        }

    }

    /**
     * Ensures the actual start and end dates are suitable given the data in the database.
     * <p>
     * See calcPlanTotals() for details of the date changes.
     *
     * @param start preferred start date - may be null
     * @param end preferred end date - may be null
     */
    protected void setActualDates(@Nullable LocalDate start, @Nullable LocalDate end) {
        actualEnd = makeEndDate(end)
        actualStart = makeStartDate(start, actualEnd)
    }

    /**
     * Adjusts the passed preferred end date, if necessary, to suit the data available in the database.
     * <p>
     * See calcPlanTotals() for details of the date changes.
     *
     * @param dt the preferred end date - may be null
     * @return an end date that is valid for the available data
     * @throws DbException if there's a problem accessing the database
     */
    @NotNull
    protected LocalDate makeEndDate(@Nullable LocalDate dt) throws DbException {
        LocalDate dbEnd = dataManager.getLastDbDate()
        if (!dbEnd) {
            throw new DbException('No data found for calculating costs - load data first.')
        }
        LocalDate desiredEnd = dt ?: dbEnd
        LocalDate dbStart = dataManager.getFirstDbDate()
        if (desiredEnd <= dbStart) {
            throw new DbException("Desired end date is before earliest data. Available data is from $dbStart to $dbEnd.")
        }
        return dbEnd < desiredEnd ? dbEnd : desiredEnd
    }

    /**
     * Adjusts the passed preferred start date, if necessary, to suit the data available in the database.
     * <p>
     * See calcPlanTotals() for details of the date changes.
     *
     * @param start the preferred start date - may be null
     * @param end a valid end date - it is recommended to use makeEndDate() to determine this
     * @return a start date that is valid for the available data
     * @throws DbException if there's a problem accessing the database
     */
    @NotNull
    protected LocalDate makeStartDate(@Nullable LocalDate start, @NotNull LocalDate end) throws DbException {
        LocalDate dbStart = dataManager.getFirstDbDate()
        if (!dbStart) {
            throw new DbException('No data found for calculating costs - load data first.')
        }
        LocalDate desiredStart = start ?: end.minusYears(1)
        LocalDate dbEnd = dataManager.getLastDbDate()
        if (desiredStart >= dbEnd) {
            throw new DbException("Desired start date is after most recent available data. Available data is from $dbStart to $dbEnd.")
        }
        return dbStart > desiredStart ? dbStart : desiredStart
    }

    /**
     * Checks the passed EnergyPlanDetails object to confirm they are valid.
     * <p>
     * The returned List contains zero or more descriptions of any problems found. This allows
     * most (all?) problems to be found in one go.
     * <p>
     * The following are checked:
     * <ul>
     *     <li>whether any plans are included
     *     <li>all plans have a supply charge
     *     <li>all plans have a set of one or more import tariffs that cover 24 hours
     *     <li>for a given 30 minute interval, all plans have only one matching import tariff
     *     <li>all export tariffs (if any) are valid
     * </ul>
     *
     * @param energyPlanDetails the list of energy plans to verify
     * @return a List of error messages, hopefully empty
     */
    @NotNull
    protected List<String> checkPlansValid(@NotNull List<EnergyPlanDetails> energyPlanDetails) {
        if (!energyPlanDetails || energyPlanDetails.isEmpty()) {
            return ['No energy plan details found.']
        }
        return energyPlanDetails.inject([]) { lst, plan ->
            if (!plan.supplyDailyCost) {
                lst << "No supply cost for plan ${plan.planName}"
            }
            lst = plan.isExportValid(lst)
            lst = plan.isImportValid(lst)
            lst
        }
    }

}
