/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.tariff.SimpleTariff
import au.yallara.energy.tariff.SteppedTariff
import au.yallara.energy.tariff.Tariff
import au.yallara.energy.tariff.TouTariff
import groovy.transform.Canonical
import groovy.transform.CompileStatic
import groovy.transform.MapConstructor
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.Month

/**
 * Holds the details of a specific energy plan.
 *
 * Normally initialised from a config file.
 */
@Canonical
@MapConstructor
@CompileStatic
class EnergyPlanDetails {
    /**
     * Name of this plan
     */
    String planName
    /**
     * Amount, in dollars, of the daily charge for supply.
     */
    BigDecimal supplyDailyCost
    /**
     * List of tariffs for feedin (exported) energy.
     */
    List<Tariff> exportTariffs = []

    /**
     * List of tariffs for imported energy.
     */
    List<Tariff> importTariffs = []

    /**
     * Equals, based only on name of plan.
     *
     * @param o object to compare to
     * @return whether the two objects are equal
     */
    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof EnergyPlanDetails)) {
            return false
        }

        EnergyPlanDetails that = (EnergyPlanDetails) o

        return planName == that.planName
    }

    /**
     * Hashcode, based only on name of plan
     *
     * @return the hashcode
     */
    @Override
    int hashCode() {
        return (planName != null ? planName.hashCode() : 0)
    }

    /**
     * Are all the export tariffs valid?
     * <p>
     *     Having no export tariffs is valid.
     *
     * @param msgs an optional List to which any error messages can be added
     * @return either the passed List, or a new List, to which any error messages have been added
     */
    @NotNull
    List<String> isExportValid(@Nullable List<String> msgs) {
        List<String> errs = msgs ?: []
        if (exportTariffs.isEmpty()) {
            return errs
        }
        return isTariffListValid(exportTariffs, errs)
    }

    /**
     * Are all the import tariffs valid?
     * <p>
     *     Unlike export tariffs, at least one import tariff must exist.
     *
     * @param msgs an optional List to which any error messages can be added
     * @return either the passed List, or a new List, to which any error messages have been added
     */
    @NotNull
    List<String> isImportValid(@Nullable List<String> msgs) {
        List<String> errs = msgs ?: []
        if (importTariffs.isEmpty()) {
            errs << "No import tariffs for plan $planName".toString()
            return errs
        }
        return isTariffListValid(importTariffs, errs)
    }

    /**
     * Checks each tariff in the passed list has been correctly initialised.
     * <p>
     *     If there are multiple tariffs, checks that there is one and only one
     *     tariff that applies to a given month, day and time.
     *     <p>
     *         If a list of Strings is supplied, any error messages will be added to the list and that
     *         list returned. Otherwise a new,
     *         possibly empty, List will be returned.
     *
     * @param tariffList the list of Tariffs to check
     * @param errs an optional List into which error messages can be added
     * @return a List containing any error messages - may be the passed list
     */
    @NotNull
    protected List<String> isTariffListValid(@NotNull List<Tariff> tariffList, @Nullable List<String> errs) {
        List<String> msg = errs == null ? [] : errs
        msg = tariffList.inject(msg) { lst, trf ->
            if (!trf.isInitialised()) {
                lst << "Tariff ${trf.getTariffName()} in plan $planName is missing information".toString()
            }
            lst
        }
        if (tariffList.size() == 1) {
            Tariff onlyTariff = tariffList[0]
            if (onlyTariff instanceof SteppedTariff || onlyTariff instanceof SimpleTariff) {
                // all ok, nothing else to check
                return msg
            }
        }
        // multiple tariffs
        // make sure there's a single tariff for all times, days, months
        Boolean anyMonths = tariffList.findResult {
            if (it instanceof TouTariff) {
                (it as TouTariff).monthsList.isEmpty() ? null : true
            } else {
                return null
            }
        }
        Boolean anyDays = tariffList.findResult {
            if (it instanceof TouTariff) {
                (it as TouTariff).daysList.isEmpty() ? null : true
            } else {
                return null
            }
        }

        List<Month> testMonthList = anyMonths ? Month.values().toList() : [null]
        List<DayOfWeek> testDayList = anyDays ? DayOfWeek.values().toList() : [null]
        testMonthList.each { Month mnth ->
            testDayList.each { DayOfWeek dy ->
                LocalTime currentTime = LocalTime.of(0, 0)
                LocalTime lastTime = LocalTime.of(23, 30)
                while (currentTime != null) {
                    List<Tariff> relevantTariff = tariffList.findAll { it.applies(mnth, dy, currentTime) }
                    if (!relevantTariff) {
                        msg << "No tariff found for month ${mnth ?: 'any'}, day ${dy ?: 'any'}, time ${currentTime} in plan ${planName}".toString()
                    } else if (relevantTariff.size() > 1) {
                        msg << "Too many tariffs (${relevantTariff.size()}) apply at month ${mnth ?: 'any'}, day ${dy ?: 'any'}, time ${currentTime} for plan ${planName}".toString()
                    }
                    currentTime = currentTime == lastTime ? null : currentTime.plusMinutes(30)
                }

            }
        }
        return msg
    }
}
