/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.exception.DbException
import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull

import java.time.LocalDate

/**
 * Generates basic statistics from the database data.
 */
@CompileStatic
class StatsMaker {
    private final DataManager dbManager

    /**
     * Constructor passed the dataManager to use for database access.
     *
     * @param dbManager the DataManager to use
     */
    StatsMaker(@NotNull DataManager dbManager) {
        this.dbManager = dbManager
    }

    /**
     * Prints a human-friendly summary of the database statistics to the passed PrintWriter.
     *
     * @param output the PrintWriter to write to
     * @throws DbException if there's a problem accessing the database.
     */
    void calculate(@NotNull PrintWriter output) throws DbException {
        List<DailyData> dailyDataList = dbManager.calcDailyData()

        LocalDate today = LocalDate.now()
        LocalDate oldestDate = dailyDataList.first().useDate
        LocalDate newestDate = dailyDataList.last().useDate
        BigDecimal numDays = new BigDecimal(newestDate - oldestDate + 1).setScale(0)
        BigDecimal totalIn = dailyDataList.sum { it.imported } as BigDecimal
        BigDecimal totalOut = dailyDataList.sum { it.exported } as BigDecimal
        BigDecimal avgIn = totalIn.divide(numDays, Util.round)
        BigDecimal avgOut = totalOut.divide(numDays, Util.round)

        String report = """
Meter ${dbManager.meterName} as at $today
Data available between $oldestDate and $newestDate

Total imported: ${Util.asKwhString(totalIn)} kWh
Total exported: ${Util.asKwhString(totalOut)} kWh
Average daily import: ${Util.asKwhString(avgIn)} kWh
Average daily export: ${Util.asKwhString(avgOut)} kWh
"""

        output.println(report)

    }
}
