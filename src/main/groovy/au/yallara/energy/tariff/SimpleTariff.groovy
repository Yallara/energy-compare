/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.tariff

import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.Month

/**
 * Represents a simple, single charge tariff that is applicable at all times.
 */
@CompileStatic
class SimpleTariff extends TariffAbstract {
    BigDecimal costPerKwh

    SimpleTariff(String name) {
        super(name)
    }

    SimpleTariff(String name, BigDecimal tariff) {
        this(name)
        costPerKwh = tariff
    }

    SimpleTariff setCostPerKwh(@NotNull BigDecimal tariff) {
        costPerKwh = tariff
        return this
    }

    /**
     * Checks whether this tariff has been correctly initialised, and is ready for use.
     * <p>
     *     This tariff requires a name and a cost per kWh.
     *
     * @return true if ready for use, else false
     */
    @Override
    Boolean isInitialised() {
        return tariffName && costPerKwh != null
    }

    /**
     * Does this tariff apply at this time and date?
     * <p>
     * Always returns true for this tariff type.
     * <p>
     * @param month month to test against, may be null.
     * @param day day to test against, may be null.
     * @param time time of day to test, may not be null
     * @return always returns true
     */
    @Override
    Boolean applies(@Nullable Month month, @Nullable DayOfWeek day, @NotNull LocalTime time) {
        return true
    }

    /**
     * Calculate the cost under this tariff for the previously set
     * number of kWh.
     *
     * @param days Ignored for this tariff type, but must not be null.
     * @return the total cost under this tariff, in dollars.
     */
    @Override
    BigDecimal calculate(@NotNull Integer days) {
        return kWh * costPerKwh
    }

    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof SimpleTariff)) {
            return false
        }

        SimpleTariff that = (SimpleTariff) o

        return tariffName == that.tariffName
    }

    @Override
    int hashCode() {
        return (tariffName != null ? tariffName.hashCode() : 0)
    }
}
