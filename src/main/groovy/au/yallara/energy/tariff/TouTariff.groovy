/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.tariff

import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.Month

/**
 * Represents a Time of Use tariff.
 * <p>
 *     This tariff type uses different tariff amounts depending on the date and time of import or export.
 */
@CompileStatic
class TouTariff extends TariffAbstract {
    protected LocalTime startTime
    protected LocalTime endTime
    protected final List<DayOfWeek> days = []
    protected final List<Month> months = []
    protected BigDecimal costPerKwh

    TouTariff(String name) {
        super(name)
    }

    /**
     * The starting and ending time of day this tariff applies. <b>All TouTariffs require both start and end times.</b>
     * <p>
     *     If your tariff varies only by month or day of week, and not by time, set the start time
     *     to midnight ('00:00'), and the end time to one minute to midnight ('23:59').
     *     <p>
     *     Times will be in local time, which is what energy companies normally quote.
     * @param start
     * @param end
     * @return
     */
    TouTariff setTimeRange(LocalTime start, LocalTime end) {
        startTime = start
        endTime = end
        return this
    }

    /**
     * Add a day of the week this tariff applies to.
     *
     * @param dy the day to add
     * @return this object, to aid fluent programming
     */
    TouTariff addDayOfWeek(@NotNull DayOfWeek dy) {
        days << dy
        return this
    }

    /**
     * Add a list of days of the week this tariff applies to.
     * <p>
     *     Existing days are not removed or replaced.
     *
     * @param dayList the list of days to add
     * @return this object, to aid fluent programming
     */
    TouTariff addDaysOfWeek(@NotNull Collection<DayOfWeek> dayList) {
        days.addAll(dayList)
        return this
    }

    /**
     * Add a month this tariff applies to.
     *
     * @param mnth the month to add
     * @return this object, to aid fluent programming
     */
    TouTariff addMonth(@NotNull Month mnth) {
        months << mnth
        return this
    }

    /**
     * Add a list of months this tariff applies to.
     * <p>
     *     Existing months are not removed or replaced.
     *
     * @param monthList the list of months to add
     * @return this object, to aid fluent programming
     */
    TouTariff addMonths(@NotNull Collection<Month> monthList) {
        months.addAll(monthList)
        return this
    }

    /**
     * Get the months this tariff applies to, as an Immutable list.
     *
     * @return an Immutable list of months
     */
    List<Month> getMonthsList() {
        return months.asImmutable()
    }

    /**
     * Get the days this tariff applies to, as an Immutable list.
     *
     * @return an Immutable list of days
     */
    List<DayOfWeek> getDaysList() {
        return days.asImmutable()
    }

    /**
     * Set the per kWh cost for this tariff, in dollars.
     *
     * @param cost the dollars per kWh applicable to this tariff
     * @return this object, to aid fluent programming
     */
    TouTariff setCostPerKwh(@NotNull BigDecimal cost) {
        costPerKwh = cost
        return this
    }

    /**
     * Get the per kWh cost for this tariff, in dollars.
     *
     * @return the kWh cost
     */
    BigDecimal getCostPerKwh() {
        return costPerKwh
    }

    /**
     * Checks whether this tariff has been correctly initialised, and is ready for use.
     * <p>
     *     This tariff requires a name, a start time, and end time and a cost per kWh.
     *
     * @return true if ready for use, else false
     */
    @Override
    Boolean isInitialised() {
        return (tariffName && startTime && endTime && costPerKwh != null)
    }

    /**
     * Whether this tariff applies at the passed date and time.
     * <p>
     *
     * @param month month to test against, may be null.
     * @param day day to test against, may be null.
     * @param time time of day to test, may not be null
     * @return whether this tariff applies under these circumstances
     */
    @Override
    Boolean applies(@Nullable Month month, @Nullable DayOfWeek day, @NotNull LocalTime time) {
        if (months) {
            if (!months.contains(month)) {
                return false
            }
        }
        if (days) {
            if (!days.contains(day)) {
                return false
            }
        }
        if (startTime && endTime) {
            return time >= startTime && time < endTime
        }
        return true
    }

    /**
     * Calculate the cost under this tariff for the previously set
     * number of kWh.
     *
     * @param days the number of days the previously set kWh covers - not used for this tariff type.
     * @return the total cost under this tariff, in dollars.
     */
    @Override
    BigDecimal calculate(@NotNull Integer days) {
        return kWh * costPerKwh
    }

    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof TouTariff)) {
            return false
        }

        TouTariff touTariff = (TouTariff) o

        if (days != touTariff.days) {
            return false
        }
        if (endTime != touTariff.endTime) {
            return false
        }
        if (months != touTariff.months) {
            return false
        }
        if (startTime != touTariff.startTime) {
            return false
        }

        return true
    }

    @Override
    int hashCode() {
        int result
        result = (startTime != null ? startTime.hashCode() : 0)
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0)
        result = 31 * result + days.hashCode()
        result = 31 * result + months.hashCode()
        return result
    }
}
