/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.tariff

import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.Month

/**
 * A common Interface for the different tariff types.
 */
@CompileStatic
interface Tariff {

    /**
     * Whether this tariff applies at the passed date and time.
     * <p>
     * @param month month to test against, may be null.
     * @param day day to test against, may be null.
     * @param time time of day to test, may not be null
     * @return true if this tariff applies under these circumstances, else false
     */
    Boolean applies(@Nullable Month month, @Nullable DayOfWeek day, @NotNull LocalTime time)

    /**
     * Whether this tariff applies at the passed date and time.
     *
     * @param date the target date, may be null
     * @param time the target time, may not be null
     * @return true if this tariff applies under these circumstances, else false
     */
    Boolean applies(@Nullable LocalDate date, @NotNull LocalTime time)

    /**
     * Calculate the cost under this tariff for the previously set
     * number of kWh.
     *
     * @param days the number of days the previously set kWh covers.
     * @return the total cost under this tariff, in dollars.
     */
    BigDecimal calculate(@NotNull Integer days)

    /**
     * Checks whether this tariff has been correctly initialised, and is ready for use.
     *
     * @return true if ready for use, else false
     */
    Boolean isInitialised()

    /**
     * Return the identifying name of this tariff.
     *
     * @return the tariff name
     */
    String getTariffName()

    /**
     * Get the current kWh of energy under this tariff.
     *
     * @return the current kWh
     */
    BigDecimal getKwh()

    /**
     * Add energy this tariffs applies to, in kWh.
     *
     * @param kwh the amount of kWh to add
     * @return this object, to aid with fluent programming style.
     */
    Tariff addKwh(BigDecimal kwh)
}
