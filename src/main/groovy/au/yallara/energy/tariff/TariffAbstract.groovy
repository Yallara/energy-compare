/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.tariff

import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.Month

/**
 * An abstract parent of all Tariff type classes.
 * <p>
 * Includes implementations of methods that tend to be common amongst different
 * tariff types.
 */
@CompileStatic
abstract class TariffAbstract implements Tariff {
    final String tariffName
    BigDecimal kWh = BigDecimal.ZERO

    /**
     * Constructor to set the name of the tariff
     *
     * @param name a name for this tariff
     */
    protected TariffAbstract(String name) {
        tariffName = name
    }

    /**
     * Get the tariff name.
     *
     * @return the tariff name
     */
    String getTariffName() {
        return tariffName
    }

    /**
     * Add energy this tariffs applies to, in kWh.
     *
     * @param kwh the amount of kWh to add
     * @return this object, to aid with fluent programming style.
     */
    Tariff addKwh(BigDecimal kWh) {
        this.kWh += kWh
        return this
    }

    /**
     * Get the current kWh of energy under this tariff.
     *
     * @return the current kWh
     */
    BigDecimal getKwh() {
        return kWh
    }

    /**
     * Whether this tariff applies at the passed date and time.
     * <p>
     *     If the date is set, extracts month and day of week from it,
     *     and uses this to call the (usually) more tariff-specific
     *     applies(Month, DayOfWeek, LocalTime) version.
     *
     * @param date the target date, may be null
     * @param time the target time, may not be null
     * @return true if this tariff applies under these circumstances, else false
     */
    Boolean applies(@Nullable LocalDate date, @NotNull LocalTime time) {
        Month month = date ? date.month : null
        DayOfWeek day = date ? date.dayOfWeek : null
        return applies(month, day, time)
    }
}
