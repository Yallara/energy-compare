/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.tariff

import groovy.transform.Canonical
import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.Month

/**
 * A stepped tariff, where the amount charged depends on the number of kWh per day.
 * <p>
 *     A common example is where an export tariff is set to a higher price for the first
 *     few kWh per days, and any energy exported after that attracts a lower tariff.
 *     <p>
 *         For example, if the first 14 kWh is at $0.10, and energy above that is at $0.049,
 *         that is represented by a SteppedTariff with two steps. One step
 *         has a cost of 0.10, a max of 14.0, and no min (or 0). The other step
 *         has a cost of 0.049, a min of 14.0, and no max.
 */
@CompileStatic
class SteppedTariff extends TariffAbstract {
    static final BigDecimal defaultMin = BigDecimal.ZERO
    static final BigDecimal defaultMax = new BigDecimal('9999')

    private final List<Step> tariffSteps = []

    /**
     * Standard constructor for this tariff.
     *
     * @param name a name for this tariff
     */
    SteppedTariff(String name) {
        super(name)
    }

    // Map in List should have fields costPerKwh, minKwh, maxKwh - last two may be absent or null
    /**
     * A convenience constructor, particularly useful for testing,
     * allowing the steps to be set at creation time.
     * <p>
     *     The second parameter must be a List of Maps. Each Map represents a step,
     *     and should have keys costPerKwh (required), minKwh (optional),
     *     maxKwh (optional). At least one of minKwh and maxKwh should be set per step.
     *     The values are BigDecimal.
     *     <p>
     * @param name a name for this tariff
     * @param steps the information about all steps
     */
    SteppedTariff(String name, List<Map<String, BigDecimal>> steps) {
        this(name)
        tariffSteps = steps.collect {
            new Step(it.costPerKwh, it.minKwh, it.maxKwh)
        }
    }

    /**
     * Whether this tariff applies at the passed date and time.
     * <p>
     *     Always returns true.
     *
     * @param month month to test against, may be null.
     * @param day day to test against, may be null.
     * @param time time of day to test, may not be null
     * @return always returns true
     */
    @Override
    Boolean applies(@Nullable Month month, @Nullable DayOfWeek day, @NotNull LocalTime time) {
        return true
    }

    /**
     * Checks whether this tariff has been correctly initialised, and is ready for use.
     * <p>
     *     This tariff requires a name and at least one step (though logically there
     *     should be at least two steps). Each step must contain a costPerKwh in dollars.
     *
     * @return true if ready for use, else false
     */
    @Override
    Boolean isInitialised() {
        if (tariffSteps.isEmpty()) {
            return false
        }
        Step emptyStep = tariffSteps.find { it.costPerKwh == null }
        return emptyStep ? false : true
    }

    /**
     * Calculate the cost under this tariff for the previously set
     * number of kWh.
     *
     * @param days the number of days the previously set kWh covers.
     * @return the total cost under this tariff, in dollars.
     */
    @Override
    BigDecimal calculate(@NotNull Integer days) {
        if (tariffSteps.isEmpty()) {
            return BigDecimal.ZERO
        }

        return tariffSteps.inject(BigDecimal.ZERO) { subtotal, step ->
            // this step cuts in at
            BigDecimal min = step.minKwh * days
            // and cuts out at
            BigDecimal max = step.maxKwh * days
            if (kWh >= min) {
                BigDecimal relevantKwh = kWh.min(max) - min
                subtotal += relevantKwh * step.costPerKwh
            }
            subtotal

        }
    }

    /**
     * Add details of a step for this tariff.
     * <p>
     *     Each step consists of:
     *     <p>
     *     <ul>
     *         <li>a dollar amount, being the cost per kWh for this step</li>
     *         <li>an optional minimum kWh this cost applies to<li>
     *             <li>an optional maximum kWh this cost applies to.
     *             </ul>
     *             <p>
     *             A normal step will have at least one of min or max set. If min is not set
     *             for a step, min will be set to 0.0. If max is not set for a step, it will be
     *             set to a large amount.
     *             <p>
     *
     * @param cost the tariff per kWh for this step
     * @param min the minimum kWh per day this step applies to
     * @param max the maximum kWh per step this step applies to
     * @return this object (to support fluent programming style)
     */
    SteppedTariff addStep(@NotNull BigDecimal cost, BigDecimal min = null, BigDecimal max = null) {
        tariffSteps << new Step(cost, min, max)
        return this
    }

    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof SteppedTariff)) {
            return false
        }

        SteppedTariff that = (SteppedTariff) o

        if (tariffSteps.size() != that.tariffSteps.size()) {
            return false
        }

        return tariffName == that.tariffName
    }

    @Override
    int hashCode() {
        return tariffName.hashCode()
    }

    /**
     * Represents a step within this tariff type.
     */
    @Canonical
    class Step {
        final BigDecimal costPerKwh
        final BigDecimal minKwh
        final BigDecimal maxKwh

        Step(@NotNull BigDecimal cost, BigDecimal min = null, BigDecimal max = null) {
            costPerKwh = cost
            minKwh = min == null ? defaultMin : min
            maxKwh = max == null ? defaultMax : max
        }

    }
}
