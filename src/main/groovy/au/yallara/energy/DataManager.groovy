/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.exception.DbException
import au.yallara.energy.exception.UnsupportedFormatException
import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.intellij.lang.annotations.Language
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

/**
 * Manages the database and most calculations that deal directly with the database.
 */
@Slf4j
class DataManager {
    protected static final String aglDateFormatStr = 'dd/MM/yyyy hh:mm:ss a'
    protected static final Locale auLocale = Locale.forLanguageTag('en-AU')
    protected static final DateTimeFormatter aglDateTimeFormat = DateTimeFormatter.ofPattern(aglDateFormatStr, auLocale)

    private final String dbFilename

    private final String meterName

    static final String streamIn = 'E1'
    static final String streamOut = 'B1'
    static final String dateDbPattern = 'yyyy-MM-dd'
    static final Integer intervals = 48

    private static final Integer sqlBatchSize = 20000

    @Language('SQL')
    private static final String insertMeterData = 'insert into meterdata(useDate, timeStart, streamId, kwhUsed) values (:usedate,:timestart,:streamid,:kwh)'

    @Language('SQL')
    private static final String summaryQuery = 'select streamId,useDate,sum(kwhUsed) as kwh from meterdata group by streamId, useDate'

    @Language('SQL') private static final String lastDateQuery = 'select max(useDate) as dt from meterdata'
    @Language('SQL') private static final String firstDateQuery = 'select min(useDate) as dt from meterdata'

    @Language('SQL') private static final String dropIndex = 'drop index IF EXISTS idx_dttm'
    @Language('SQL') private static final String createIndex = 'create index idx_dttm on meterdata (useDate, timeStart)'

    @Language('SQLite') protected static final String initSql = '''
create table meterdata
(
    useDate   DATE    NOT NULL,
    timeStart TIME    NOT NULL,
    streamId  char(3) not null,
    kwhUsed   float default 0.000,
    primary key (streamId, useDate, timeStart)
);
'''

    final List<String> pragmas = [
      'PRAGMA page_size = 8192',
      'PRAGMA cache_size = 3000',
    ]

    /**
     * Constructor, passed location of Sqlite database and name for meter.
     *
     * Database will be in a file named after the meter, in the specified directory.
     *
     * @param dbPath the path to the directory to contain the database file
     * @param name the name of the meter.
     */
    DataManager(@NotNull String dbPath, @NotNull String name) {
        dbFilename = Util.filePath("${dbPath}/${name}.db")
        meterName = name
    }

    /**
     * Return the name of the meter.
     *
     * @return the meter name
     */
    @NotNull
    String getMeterName() {
        return meterName
    }

    /**
     * Determine whether associated energy is imported or exported, based on the passed stream name.
     *
     * @param streamId the name of the stream from the interval meter data file.
     * @return EnergyDirection specifying whether energy is being imported or exported. null if unknown.
     */
    @Nullable
    EnergyDirection getDirection(@Nullable String streamId) {
        if (streamId == streamOut) {
            return EnergyDirection.OUT
        }
        if (streamId == streamIn) {
            return EnergyDirection.IN
        }
        return null
    }

    /**
     * Return a Sql object linked to the current Sqlite database.
     * <p>
     * If the database already exists, simply returns the Sql object irrespective
     * of the value of the 'create' parameter.
     * <p>
     * If the database does not exist, and 'create' is false, returns null.
     * <p>
     * If the database does not exist, and 'create' is true, creates a database and initialises it.
     *
     * @param create whether to create the database if it doesn't already exist
     * @return an Sql object associated with the database, or null
     */
    @Nullable
    Sql openDb(Boolean create = false) {
        Sql sql = null
        File dbFile = new File(dbFilename)
        if (dbFile.canRead()) {
            log.info("Opened existing db $dbFilename")
            sql = Sql.newInstance("jdbc:sqlite:$dbFilename", 'org.sqlite.JDBC')
            usePragmas(sql)
        } else if (create) {
            log.info("Creating new db $dbFilename")
            sql = Sql.newInstance("jdbc:sqlite:$dbFilename", 'org.sqlite.JDBC')
            usePragmas(sql)
            sql.execute(initSql)
        }
        return sql
    }

    /**
     * Set some Sqlite pragmas to increase speed.
     *
     * @param sql an Sql object connected to the Sqlite database
     */
    private void usePragmas(Sql sql) {
        pragmas.each { pragma ->
            sql.execute(pragma)
        }
    }

    /**
     * Loads data from the database, arranged as total import and export per day.
     *
     * @return a List of DailyData objects
     * @throws DbException if the database does not exist
     */
    @NotNull
    List<DailyData> calcDailyData() throws DbException {
        Sql sql = openDb()
        if (!sql) {
            throw new DbException("Could not find database at $dbFilename")
        }

        try {
            def summaryList = sql.rows(summaryQuery)

            Map<String, DailyData> dataSummaryMap = [:].withDefault { new DailyData() }

            summaryList.each { entry ->
                DailyData dailyData = dataSummaryMap[(entry.useDate.toString())]
                if (entry.streamId == streamOut) {
                    dailyData.exported = entry.kwh
                } else {
                    dailyData.imported = entry.kwh
                }
                dailyData.useDate = entry.useDate
            }

            return dataSummaryMap.isEmpty() ? [] : dataSummaryMap.values().sort { it.useDate }

        } finally {
            closeSql(sql)
        }
    }

    /**
     * Imports data from an <i>AEMO MDFF NEM12 csv</i> OR <i>AGL-type csv</i> data file into the database.
     *
     * @param inReader a Reader that will provide the CSV data
     * @param timezoneSupport a TimezoneSupport object that knows the local timezone and time offset - needed for AGL data format
     * @throws UnsupportedFormatException if the data file format is unsupported
     * @throws DbException if there's a problem accessing the database
     */
    void importData(@NotNull Reader inReader, @NotNull TimezoneSupport timezoneSupport) throws DbException, UnsupportedFormatException {
        // first determine format we are dealing with
        CSVReader reader = new CSVReaderBuilder(inReader).build()

        String[] data

        // look at first line to determine data format
        data = reader.readNext()
        if (data[0] == 'AccountNumber') {
            importAglFormat(reader, timezoneSupport)
        } else if (data[0] == '200') {
            importNEM12Format(reader, data)
        } else {
            throw new UnsupportedFormatException('Could not determine format of data file. Not importing.')
        }
    }

    /**
     * Reads CSV data in AGL format from a CSVReader into the database.
     *
     * @param reader a CSVReader from which we can read the AGL-formatted data. The first line (headers) has already been read and will be discarded.
     * @param timezoneSupport a TimezoneSupport object that knows the local timezone and time offset
     * @throws UnsupportedFormatException thrown if the data file format does not meet expectations
     * @throws DbException thrown if there is an error accessing the database
     */
    protected void importAglFormat(@NotNull CSVReader reader, @NotNull TimezoneSupport timezoneSupport)
      throws UnsupportedFormatException, DbException {
        Sql sql = openDb(true)
        if (!sql) {
            throw new DbException("Unable to open or create database at $dbFilename")
        }

        String[] data
        Integer lineNum = 0

        try {
            LocalDate lastDbDate = getLastDbDate(sql)
            sql.execute(dropIndex) // for speed
            sql.withTransaction {
                sql.withBatch(sqlBatchSize, insertMeterData) { ps ->
                    // skip first line, which is already read, it's a header
                    while ((data = reader.readNext()) != null) {
                        // weird operations on datetime needed because of Java's ideas as to when AM and PM are allowed to be upper or lower case.
                        String startDateTimeStr = data[6].toLowerCase()
                        LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeStr, aglDateTimeFormat)
                        LocalDateTime convertedStartTime = timezoneSupport.zoneToOffset(startDateTime)
                        // loaded time is zone time, perhaps with DST, but we want standard time for the database
                        // AGL uses local timezone, not standard time, so daylight savings changes are treated strangely
                        // Looks like there are missing times at DST start, and the times that should be duplicated at the
                        // end instead have summed values, e.g. what should be two values at the two different 2.30AM of 0.056 and 0.048
                        // are instead one value of 0.104. This is assumed based on actual data showing the duplicated times import roughly
                        // twice the kWh of surrounding "normal" times.
                        LocalDate startDate = convertedStartTime.toLocalDate()
                        if (startDate > lastDbDate) {
                            String useDate = convertedStartTime.format('yyyy-MM-dd')
                            String startTime = convertedStartTime.format('HH:mm')
                            String streamId = extractAglStream(data[4])
                            String kwh = data[8]
                            ps.addBatch([usedate: useDate, timestart: startTime, streamid: streamId, kwh: kwh])
                        }
                        lineNum++
                        if ((lineNum % 100) == 0) {
                            log.info("Imported $lineNum lines")
                        }
                    }
                }
            }
            sql.execute(createIndex)
            log.warn("Imported $lineNum lines of data")
        } finally {
            closeSql(sql)
        }
    }

    /**
     * Reads NEM12 formated data from a CSVReader into the database. The first line has already been read,
     * and is passed in <i>firstLine</i>
     *
     * @param reader the CSVReader to read the data from
     * @param firstLine the first line extracted from the CSV file
     * @throws UnsupportedFormatException thrown if the data file format does not meet expectations
     * @throws DbException thrown if there is an error accessing the database
     */
    protected void importNEM12Format(@NotNull CSVReader reader, @NotNull String[] firstLine) throws UnsupportedFormatException, DbException {
        Sql sql = openDb(true)
        if (!sql) {
            throw new DbException("Unable to open or create database at $dbFilename")
        }

        String[] data = firstLine
        String streamId = ''
        Integer intervalLength = 30
        Integer linenum = 0

        try {
            String lastDbDate = getLastDbDateCsv(sql)
            sql.execute(dropIndex) // for speed
            sql.withTransaction {
                while (data != null) {
                    switch (data[0]) {
                        case '200':
                            streamId = data[3]
                            intervalLength = data[8] as Integer
                            break
                        case '300':
                            processNEM12Line(sql, lastDbDate, streamId, intervalLength, data)
                            break
                        case '400':
                            // ignore
                            break
                        default:
                            throw new UnsupportedFormatException("Unknown line in meter interval data with first column ${data[0]} on line $linenum")
                    }
                    linenum++
                    if ((linenum % 100) == 0) {
                        log.info("Imported $linenum lines")
                    }
                    data = reader.readNext()
                }
            }
            sql.execute(createIndex)
            log.warn("Imported $linenum")
        } finally {
            closeSql(sql)
        }
    }

    /**
     * Processes, and stores in database, a single '300' NEM12-format line from the CSV data.
     *
     * @param sql the open database connection to use
     * @param lastEntryDate the date of the current last entry in the database. Data <= this will not be processed/stored
     * @param streamId the streamId for this data, read from previous '200' entry, identifies whether data is import or output
     * @param intervalLength the number of minutes between data points
     * @param data an array of the line read from the CSV
     */
    protected void processNEM12Line(@NotNull Sql sql, @NotNull String lastEntryDate, @NotNull String streamId,
                                    @NotNull Integer intervalLength, @NotNull String[] data) {
        if (data[1] > lastEntryDate) {
            String csvDate = toDbDate(data[1])
            Integer lastCol = 2 + intervals - 1
            Integer maxColNum = data.size() - 1
            // in total there are intervals datapoints in each row, starting from the third column

            // first data point is at midnight, then each data point
            // is at intervalLength minutes after the last
            LocalTime csvTime = LocalTime.of(0, 0, 0)

            sql.withBatch(sqlBatchSize, insertMeterData) { ps ->
                (2..lastCol).each { colNum ->
                    // Ausnet data may truncate rows, and include QualityMethod V in the wrong place,
                    // contrary to the specification (see Appendix C, Quality Flag V, in the specification)
                    // In this case, assume 0 for remaining values
                    def val = (colNum > maxColNum) || data[colNum] == 'V' ? '0' : data[colNum]
                    def tm = csvTime.format('HH:mm:ss')
                    ps.addBatch([usedate: csvDate, timestart: tm, streamid: streamId, kwh: val])
                    csvTime = csvTime.plusMinutes(intervalLength)
                }
            }
        }
    }

    /**
     * Converts date as included in NEM12 CSV (yyyyMMdd) into SQL-compatible date (yyyy-MM-dd)
     *
     * @param data the date as read from the CSV (yyyyMMdd)
     * @return the date in SQL-compatible format
     * @throws IllegalArgumentException thrown if the passed String is not in the expected format
     */
    @NotNull
    protected String toDbDate(@Nullable String dta) throws IllegalArgumentException {
        String data = dta?.trim()
        if (!data || data.length() != 8) {
            throw new IllegalArgumentException("date string not right format: $data")
        }
        return "${data[0..3]}-${data[4..5]}-${data[6..7]}"
    }

    /**
     * Retrieves the earliest date of data in the database.
     *
     * @return the oldest date in the database
     * @throws DbException if there's a problem accessing the database
     */
    @Nullable
    LocalDate getFirstDbDate() throws DbException {
        return getDbDate(firstDateQuery)
    }

    /**
     * Retrieves the most recent date of data in the database.
     * <p>
     *     If an open Sql connection is passed, will use it and not close it. If no
     *     connection is supplied, will open one and close when finished.
     *
     * @param sql optional Sql connection to use
     * @return the newest date in the database
     * @throws DbException if there's a problem accessing the database
     */
    @Nullable
    LocalDate getLastDbDate(Sql sql = null) throws DbException {
        return getDbDate(lastDateQuery, sql)
    }

    /**
     * Retrieves a date from the database as a LocalDate, using the passed query.
     *
     * @param query the query that will select the desired date as variable 'dt'
     * @param sql optional Sql connection to use - will create if not supplied
     * @return the relevant LocalDate, or null if not found
     * @throws DbException if error accessing database
     */
    @Nullable
    protected LocalDate getDbDate(String query, Sql sqlIn = null) throws DbException {
        Sql sql = sqlIn ?: openDb()
        if (!sql) {
            throw new DbException("Could not find database at $dbFilename")
        }

        try {
            def row = sql.firstRow(query)
            if (row?.dt) {
                return LocalDate.parse(row.dt.toString())
            }
            return null
        } finally {
            if (!sqlIn) {
                closeSql(sql)
            }
        }
    }

    /**
     * Fetches the most recent date of database data, formatted as a String suitable for comparing with the dates in the AEMO MDFF NEM12 csv.
     *
     * @param sql the Sql object associated with the database
     * @return a suitable String representation of the date
     */
    @NotNull
    protected String getLastDbDateCsv(@NotNull Sql sql) {
        def row = sql.firstRow(lastDateQuery)
        def dt = row?.dt ? row.dt.toString() : '0000-00-00'
        return dt.replaceAll('-', '')
    }

    /**
     * Helper to close the database connection
     *
     * @param sql the database connection to close
     */
    void closeSql(@Nullable Sql sql) {
        if (sql) {
            sql.close()
        }
    }

    /**
     * Extracts the stream name from the relevant field in the AGL data.
     * <p>
     *     The passed String should have a format similar to '123456#E1' where
     *     'E1' is the stream.
     *
     * @param data the String containing the stream name
     * @return The stream id/name
     */
    protected String extractAglStream(String data) {
        String[] sections = data.split('#')
        return sections[1]
    }
}
