/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy.exception

/**
 * Exception thrown if there are errors in the imported energy
 * plan information.
 *
 */
class PlanFormatException extends Exception {
    List<String> problemList

    /**
     * Constructor that accepts a single message.
     *
     * Parent Exception class message is actually hard-coded
     * to 'Errors found in Plan configuration', and passed
     * message is added to this class' list of error messages.
     *
     * @param msg String describing the error
     */
    PlanFormatException(String msg) {
        super('Errors found in Plan configuration')
        problemList = [msg]
    }

    /**
     * Constructor that accepts a List of String messages.
     *
     * This allows multiple errors in a plan file
     * to be reported at once.
     *
     * @param messages a List of error messages
     */
    PlanFormatException(List<String> messages) {
        super('Errors found in Plan configuration')
        problemList = messages
    }

    /**
     * Retrieves a message describing the causes of this Exception.
     *
     * Returns a multi-line String which starts with
     * the message held by the parent Exception,
     * then includes all the passed error messages, each
     * on a new line.
     *
     * @return a String describing the causes of the Exception.
     */
    @Override
    String getMessage() {
        return """
${super.getMessage()}
${problemList.join(System.lineSeparator())}
"""
    }
}
