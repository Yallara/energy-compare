/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.math.RoundingMode
import java.sql.Time
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.Month
import java.time.format.DateTimeParseException

/**
 * Misc small utility methods and constants.
 */
@CompileStatic
@Slf4j
class Util {

    /**
     * Number of decimal places to use when outputting kWh values.
     */
    static final Integer kwhScale = 1
    /**
     * Number of decimal places to use when outputting dollar amounts.
     */
    static final Integer currencyScale = 2
    /**
     * Rounding mode to use when outputting scaled values.
     */
    static final RoundingMode round = RoundingMode.HALF_UP

    static String baseDir = './'


    @NotNull
    static String filePath(String target) {
        String result = "${baseDir}${target}"
        return result
    }
    /**
     * Converts the passed object to a BigDecimal, possibly via toString(). Guarantees to return a valid BigDecimal, or throw NumberFormatException.
     *
     * @param object the object to convert
     * @return a BigDecimal representation of the value, if possible. For null, return ZERO.
     * @throws NumberFormatException if the passed value could not be converted to a BigDecimal, for example, a String with non-numerics.
     */
    @NotNull
    static BigDecimal asBigDecimal(@Nullable Object object) throws NumberFormatException {
        if (!object) {
            return BigDecimal.ZERO
        }
        if (object instanceof BigDecimal) {
            return object as BigDecimal
        }
        return new BigDecimal(object.toString())
    }

    /**
     * Converts the passed object to a LocalTime object.
     * <p>
     * If the object is null, throws NumberFormatException.
     * <p>
     * If it is a LocalTime, LocalDateTime or java.sql.Time, converts the Time only portion to LocalTime and returns that.
     * <p>
     * Otherwise, attempts to convert the toString() version of the object to a LocalTime.
     * <p>
     *     There is no adjustment for Daylight Savings or anything else. This method is for converting object type, not value.
     *
     * @param object the object to be converted
     * @return a LocalTime representation of the object
     * @throws NumberFormatException if the object is null or cannot be converted to a LocalTime
     */
    @NotNull
    static LocalTime asLocalTime(@Nullable Object object) throws NumberFormatException {
        if (!object) {
            throw new NumberFormatException('Cannot convert null to a LocalDate')
        }
        if (object instanceof LocalTime) {
            return object as LocalTime
        }
        if (object instanceof LocalDateTime) {
            return (object as LocalDateTime).toLocalTime()
        }
        if (object instanceof Time) {
            return (object as Time).toLocalTime()
        }
        try {
            return LocalTime.parse(object.toString())
        } catch (DateTimeParseException ignored) {
            throw new NumberFormatException("Could not convert String of ${object} to LocalTime")
        }
    }

    /**
     * Formats the passed BigDecimal as currency, including $ symbol.
     *
     * Returns the empty String if the passed value is null.
     *
     * @param valBD the value to format
     * @return the formatted value
     */
    @NotNull
    static String asCurrencyString(@Nullable BigDecimal valBD) {
        if (valBD == null) {
            return ''
        }
        return String.format('\$%,.2f', valBD.setScale(currencyScale, Util.round))
    }

    /**
     * Formats the passed BigDecimal as kWh. Does not include the units.
     *
     * Returns the empty String if the passed value is null.
     *
     * @param valBD the value to format
     * @return the formatted value
     */
    @NotNull
    static String asKwhString(@Nullable BigDecimal kwh) {
        if (kwh == null) {
            return ''
        }
        return String.format('%,.1f', kwh.setScale(kwhScale, Util.round))
    }

    /**
     * Converts a String containing one or more comma-separated day names into
     * a Collection of DayOfWeek.
     * <p>
     *     Each name must be the full English day name. For example, 'Monday,TUESDAY,friday' will be converted.
     *     'Mon,TUE' will throw an exception.
     *     <p>
     *     The result will not contain any duplicates.
     *
     * @param daysStr a String with comma-separated day names
     * @return a Collection of matching DayOfWeek objects
     * @throws IllegalArgumentException if the passed String is not in the expected format
     */
    @NotNull
    static Collection<DayOfWeek> asDaysOfWeek(@Nullable String daysStr) throws IllegalArgumentException {
        if (!daysStr) {
            return []
        }

        List<String> daysStrList = daysStr.split(',').collect { it.trim().toUpperCase() }
        return daysStrList.collect {
            try {
                DayOfWeek.valueOf(it)
            } catch (IllegalArgumentException iae) {
                throw new IllegalArgumentException("$it is not a recognised Day name")
            }
        }.unique()
    }

    /**
     * Converts a String containing one or more comma-separated month names into
     * a Collection of Month.
     * <p>
     *     Each name must be the full English month name. For example, 'March,APRIL,may' will be converted.
     *     'Mar,APR' will throw an exception.
     *     <p>
     *     The result will not contain any duplicates.
     *
     * @param monthsStr a String with comma-separated month names
     * @return a Collection of matching Month objects
     * @throws IllegalArgumentException if the passed String is not in the expected format
     */
    @NotNull
    static Collection<Month> asMonths(@Nullable String monthsStr) throws IllegalArgumentException {
        if (!monthsStr) {
            return []
        }

        List<String> monthsStrList = monthsStr.split(',').collect { it.trim().toUpperCase() }
        return monthsStrList.collect {
            try {
                Month.valueOf(it)
            } catch (IllegalArgumentException iae) {
                throw new IllegalArgumentException("$it is not a recognised Month name")
            }
        }.unique()
    }
}
