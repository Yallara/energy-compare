/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.intellij.lang.annotations.Language
import org.jetbrains.annotations.NotNull

import java.time.LocalDate
import java.time.LocalTime

/**
 * Manages calculation of the cost of an energy plan.
 */
@Slf4j
class CostCalculator {

    private final TimezoneSupport timezoneSupport

    private final DataManager dataManager

    @Language('SQL') private final String usageQuery = 'select streamId, useDate, kwhUsed, timeStart from meterdata where useDate = :usedate'

    /**
     * Constructor.
     *
     * @param dataManager the DataManager to use to access the database
     * @param timezoneSupport a TimezoneSupport object initialised with the local Timezone and UTC offset
     */
    CostCalculator(@NotNull DataManager dataManager, @NotNull TimezoneSupport timezoneSupport) {
        this.dataManager = dataManager
        this.timezoneSupport = timezoneSupport
    }

    /**
     * Calculate the cost of a specific plan using the data in the database between the passed dates.
     * <p>
     * The database must contain valid energy data for the specified date range. This is normally
     * assured by the code that calls this method.
     *
     * @param energyPlanDetails the energy plan to use
     * @param startDate the start date of the target database data
     * @param endDate the end date of the target database data
     * @return the total cost of this plan for the specified date range, in dollars
     */
    @NotNull
    BigDecimal calculate(@NotNull EnergyPlanDetails energyPlanDetails, @NotNull LocalDate startDate, @NotNull LocalDate endDate) {
        CostBreakdown costBreakdown = calculateBreakdown(energyPlanDetails, startDate, endDate)
        return costBreakdown.total
    }

    /**
     * Creates a CostBreakdown object containing a cost breakdown of this energy plan, over the
     * specified date range.
     * <p>
     * The database must contain valid energy data for the specified date range. This is normally
     * assured by the code that calls this method.
     *
     * @param energyPlanDetails the energy plan to use
     * @param startDate the start date of the target database data
     * @param endDate the end date of the target database data
     * @return the populated CostBreakdown object
     */
    @NotNull
    CostBreakdown calculateBreakdown(@NotNull EnergyPlanDetails energyPlanDetails, @NotNull LocalDate startDate, @NotNull LocalDate endDate) {
        Sql sql = dataManager.openDb()

        try {
            CostBreakdown costBreakdown = new CostBreakdown(energyPlanDetails, startDate, endDate)
            Integer numDays = 0

            (startDate..endDate).each { aDay ->
                sql.eachRow(usageQuery, [usedate: aDay]) { row ->
                    EnergyDirection direction = dataManager.getDirection(row.streamId)
                    LocalTime adjustedStartTime = timezoneSupport.offsetToZone(aDay, row.timeStart)
                    BigDecimal kwh = Util.asBigDecimal(row.kwhUsed)
                    costBreakdown.addKwh(direction, aDay, adjustedStartTime, kwh)
                }
                numDays++
            }
            costBreakdown.days = numDays
            return costBreakdown
        } finally {
            dataManager.closeSql(sql)
        }
    }
}
