/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate

/**
 * Simple object representing the energy data for a single day.
 *
 * Normally used to represent the database data for a single day.
 */
@CompileStatic
class DailyData implements Comparable<DailyData> {
    LocalDate useDate
    BigDecimal imported
    BigDecimal exported

    void setUseDate(@Nullable Object dt) {
        if (dt instanceof LocalDate) {
            useDate = dt
        } else {
            useDate = dt ? LocalDate.parse(dt.toString(), DataManager.dateDbPattern) : null
        }
    }

    @Nullable
    LocalDate getUseDate() {
        return useDate
    }

    void setImported(@Nullable imp) {
        imported = Util.asBigDecimal(imp)
    }

    @NotNull
    BigDecimal getImported() {
        return imported ?: BigDecimal.ZERO
    }

    void setExported(@Nullable exp) {
        exported = Util.asBigDecimal(exp)
    }

    @NotNull
    BigDecimal getExported() {
        return exported ?: BigDecimal.ZERO
    }

    /**
     * Equals, based on useDate, imported and exported.
     * @param o the object to compare with
     * @return whether the two objects are equal
     */
    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof DailyData)) {
            return false
        }

        DailyData dailyData = (DailyData) o

        if (exported != dailyData.exported) {
            return false
        }
        if (imported != dailyData.imported) {
            return false
        }
        if (useDate != dailyData.useDate) {
            return false
        }

        return true
    }

    /**
     * Hashcode, based on useDate, imported, exported.
     *
     * @return the hashcode
     */
    @Override
    int hashCode() {
        int result
        result = (useDate != null ? useDate.hashCode() : 0)
        result = 31 * result + imported.hashCode()
        result = 31 * result + exported.hashCode()
        return result
    }

    /**
     * For sorting, based only on useDate
     *
     * @param other the object to compare with
     * @return sort order indication
     */
    @Override
    int compareTo(@NotNull DailyData other) {
        return useDate <=> other.useDate
    }
}
