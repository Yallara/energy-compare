/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import spock.lang.Specification

import java.math.RoundingMode


class IntegrationSpec extends Specification {
    static final String dbFilename = 'integTestDb'
    static final String dbPath = '.'
    DataManager dataManager
    TimezoneSupport tzs

    def setup() {
        dataManager = new DataManager(dbPath, dbFilename)
        tzs = new TimezoneSupport(null,null)
    }

    def cleanup() {
        new File("${dbPath}/${dbFilename}.db").delete()
    }

    def 'test plan stats with lots of data in NEM12 format'() {
        given:
        Reader reader1 = new StringReader(TestData.data1)
        Reader reader2 = new StringReader(TestData.data2)
        StatsMaker statsMaker = new StatsMaker(dataManager)
        StringWriter writer = new StringWriter(1024)

        when:
        dataManager.importData(reader1, tzs)
        dataManager.importData(reader2, tzs)
        statsMaker.calculate(new PrintWriter(writer))
        def result = writer.toString()

        then:
        result.contains('Total exported: 1,218.8 kWh')
        result.contains('Total imported: 10.5 kWh')
    }

    def 'test plan stats with 3 days data NEM12'() {
        given:
        Reader reader = new StringReader(TestData.dataShort)
        StatsMaker statsMaker = new StatsMaker(dataManager)
        StringWriter writer = new StringWriter(1024)

        when:
        dataManager.importData(reader, tzs)
        statsMaker.calculate(new PrintWriter(writer))
        def result = writer.toString()

        then:
        result.contains('Total exported: 18.3 kWh')
        result.contains('Total imported: 42.2 kWh')
    }

    def 'test plan stats with 3 days Ausnet data NEM12'() {
        given:
        Reader reader = new StringReader(TestData.ausnetDodgyData)
        StatsMaker statsMaker = new StatsMaker(dataManager)
        StringWriter writer = new StringWriter(1024)

        when:
        dataManager.importData(reader, tzs)
        statsMaker.calculate(new PrintWriter(writer))
        def result = writer.toString()

        then:
        result.contains('Total exported: 0.0 kWh')
        result.contains('Total imported: 67.9 kWh')
    }

    def 'test plan stats with lots of AGL data'() {
        given:
        Reader reader1 = new StringReader(TestData.aglData1)
        Reader reader2 = new StringReader(TestData.aglData2)
        Reader reader3 = new StringReader(TestData.aglData3)
        Reader reader4 = new StringReader(TestData.aglData4)
        StatsMaker statsMaker = new StatsMaker(dataManager)
        StringWriter writer = new StringWriter(1024)

        when:
        dataManager.importData(reader1, tzs)
        dataManager.importData(reader2, tzs)
        dataManager.importData(reader3, tzs)
        dataManager.importData(reader4, tzs)
        statsMaker.calculate(new PrintWriter(writer))
        def result = writer.toString()

        then:
        result.contains('Total exported: 151.1 kWh')
        result.contains('Total imported: 40.1 kWh')

    }

    def 'test plan compare'() {
        given:
        Reader reader = new StringReader(TestData.dataShort)
        dataManager.importData(reader, tzs)
        PlanComparator comparator = new PlanComparator(dataManager, tzs)
        ConfigObject conf = new ConfigSlurper().parse(simpleConfig)
        List<EnergyPlanDetails> plans = comparator.energyPlanConfToData(conf)
        comparator.plans.addAll(plans)

        when:
        Map<String, BigDecimal> costs = comparator.calcPlanTotals(null, null)

        then:
        costs.size() == 1
        costs.testPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('17.35')
    }

    def 'test complex plan compare'() {
        given:
        Reader reader = new StringReader(TestData.dataShort)
        dataManager.importData(reader, tzs)
        PlanComparator comparator = new PlanComparator(dataManager, tzs)
        ConfigObject conf = new ConfigSlurper().parse(complexConfig)
        List<EnergyPlanDetails> plans = comparator.energyPlanConfToData(conf)
        comparator.plans.addAll(plans)

        when:
        Map<String, BigDecimal> costs = comparator.calcPlanTotals(null, null)

        then:
        costs.size() == 3
        costs.firstPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('14.72')
        costs.secondPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('16.33')
        costs.thirdPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('18.53')
    }

    def 'test complex plan compare with summer data'() {
        given:
        Reader reader = new StringReader(TestData.dataShortSummer)
        dataManager.importData(reader, tzs)
        PlanComparator comparator = new PlanComparator(dataManager, tzs)
        ConfigObject conf = new ConfigSlurper().parse(complexConfig)
        List<EnergyPlanDetails> plans = comparator.energyPlanConfToData(conf)
        comparator.plans.addAll(plans)

        when:
        Map<String, BigDecimal> costs = comparator.calcPlanTotals(null, null)

        then:
        costs.size() == 3
        costs.firstPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('14.32')
        costs.secondPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('16.33')
        costs.thirdPlan.setScale(2, RoundingMode.HALF_UP) == new BigDecimal('18.02')
    }


    static final String simpleConfig = '''
testPlan {
  supplyCost = 1.2134
  importTariffs = [
    [
      name: 'only Tariff',
      tariff: 0.3245
    ]
  ]
}
'''

    static final String complexConfig = '''
firstPlan {
  supplyCost = 1.234
  importTariffs = [
    [
      name: 'peak',
      startTime: '15:00',
      endTime: '21:00',
      tariff: 0.432
    ],
    [
      name: 'offpeak1',
      startTime: '21:00',
      endTime: '23:59',
      tariff: 0.2134
    ],
    [
      name: 'offpeak2',
      startTime: '00:00',
      endTime: '06:00',
      tariff: 0.2134
    ],
    [
      name: 'shoulder',
      startTime: '06:00',
      endTime: '15:00',
      tariff: 0.2567
    ]
  ]
  exportTariffs = [
    [
      max: 14,
      tariff: 0.12
    ],
    [
      min:14,
      tariff: 0.049
    ]
  ]
}
secondPlan {
  supplyCost = 1.2134
  importTariffs = [
    [
      name: 'only Tariff',
      tariff: 0.3245
    ]
  ]
  exportTariffs = [
    [
      tariff: 0.056
    ]
  ]
}
thirdPlan {
  supplyCost = 0.732
  importTariffs = [
    [
      name: 'Peak',
      startTime: '16:00',
      endTime: '23:59',
      tariff: 0.4732
    ],
    [
      name: 'Off Peak',
      startTime: '00:00',
      endTime: '16:00',
      tariff: 0.211
    ]
  ]
}
'''

}
