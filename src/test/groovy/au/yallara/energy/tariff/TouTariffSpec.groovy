/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy.tariff

import spock.lang.Specification

import java.math.RoundingMode
import java.time.DayOfWeek
import java.time.LocalTime
import java.time.Month


class TouTariffSpec extends Specification {

    def 'isInitialised works as intended'() {
        given:
        TouTariff tariff = new TouTariff('test')
        tariff.setTimeRange(start, end)
        tariff.addDaysOfWeek(days)
        tariff.addMonths(months)
        tariff.setCostPerKwh(cost ? new BigDecimal(cost) : null)

        when:
        Boolean result = tariff.isInitialised()

        then:
        result == expected

        where:
        start               | end                | days               | months      | cost   | expected
        LocalTime.of(1, 30) | LocalTime.of(2, 0) | []                 | []          | '0.23' | true
        null                | LocalTime.of(2, 0) | []                 | []          | '0.23' | false
        LocalTime.of(1, 30) | null               | []                 | []          | '0.23' | false
        LocalTime.of(1, 30) | LocalTime.of(2, 0) | [DayOfWeek.FRIDAY] | []          | '0.23' | true
        LocalTime.of(1, 30) | LocalTime.of(2, 0) | []                 | [Month.MAY] | '0.23' | true
        LocalTime.of(1, 30) | LocalTime.of(2, 0) | []                 | []          | null   | false
    }

    def 'applies correctly says whether the tariff applies'() {
        given:
        TouTariff tariff = new TouTariff('test')
        tariff.setTimeRange(start, end)
        tariff.addDaysOfWeek(days)
        tariff.addMonths(months)

        when:
        Boolean result = tariff.applies(mnth, dy, tm)

        then:
        result == expected

        where:
        start              | end                | days               | months      | mnth       | dy               | tm                 | expected
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | []                 | []          | Month.MAY  | DayOfWeek.FRIDAY | LocalTime.of(1, 0) | true
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | []                 | []          | Month.MAY  | DayOfWeek.FRIDAY | LocalTime.of(9, 0) | false
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | [DayOfWeek.MONDAY] | []          | Month.MAY  | DayOfWeek.FRIDAY | LocalTime.of(1, 0) | false
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | [DayOfWeek.FRIDAY] | []          | Month.MAY  | DayOfWeek.FRIDAY | LocalTime.of(1, 0) | true
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | []                 | [Month.MAY] | Month.MAY  | DayOfWeek.FRIDAY | LocalTime.of(1, 0) | true
        LocalTime.of(1, 0) | LocalTime.of(9, 0) | []                 | [Month.MAY] | Month.JULY | DayOfWeek.FRIDAY | LocalTime.of(1, 0) | false
    }

    def 'calculates cost correctly'() {
        given:
        TouTariff tariff = new TouTariff('test').setCostPerKwh(new BigDecimal(cost))
        tariff.setkWh(new BigDecimal(kwh))

        when:
        BigDecimal result = tariff.calculate(days)

        then:
        result.setScale(2, RoundingMode.HALF_UP) == new BigDecimal(expected)

        where:
        cost     | kwh     | days | expected
        '0.4325' | '23.43' | 5    | '10.13'
        '0.4325' | '23.43' | 50   | '10.13'
        '0.0'    | '23.43' | 5    | '0.0'


    }
}
