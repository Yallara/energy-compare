/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy.tariff

import spock.lang.Specification


class SimpleTariffSpec extends Specification {

    def 'isInitialised works as intended'() {
        given:
        SimpleTariff tariff = new SimpleTariff(name).setCostPerKwh(cost)

        when:
        Boolean result = tariff.isInitialised()

        then:
        result == expected

        where:
        name   | cost                   | expected
        'test' | new BigDecimal('0.56') | true
        null   | new BigDecimal('0.56') | false
        'test' | null                   | false
        'test' | BigDecimal.ZERO        | true
        null   | null                   | false
    }

    def 'cost calculated correctly'() {
        given:
        SimpleTariff tariff = new SimpleTariff('test').setCostPerKwh(new BigDecimal(cost)).addKwh(new BigDecimal(kwh))

        when:
        BigDecimal result = tariff.calculate(days)

        then:
        result == new BigDecimal(expected)

        where:
        cost   | kwh    | days | expected
        '0.56' | '3.54' | 2    | '1.9824'
        '0.56' | '3.54' | 4    | '1.9824'
        '0.0'  | '3.54' | 4    | '0.0'
        '0.56' | '0.0'  | 4    | '0.0'


    }
}
