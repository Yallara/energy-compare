/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy.tariff

import spock.lang.Specification

import java.math.RoundingMode


class SteppedTariffSpec extends Specification {

    def 'isInitialised works when correctly initialised'() {
        given:
        SteppedTariff tariff = new SteppedTariff('test')
        tariff.addStep(new BigDecimal('0.049'), min, max)

        when:
        Boolean result = tariff.isInitialised()

        then:
        result == true

        where:
        min                  | max
        new BigDecimal('0')  | new BigDecimal('12')
        null                 | new BigDecimal('12')
        new BigDecimal('14') | null
    }

    def 'tariff without steps is not considered initialised'() {
        given:
        SteppedTariff tariff = new SteppedTariff('test')

        when:
        Boolean result = tariff.isInitialised()

        then:
        result == false
    }

    def 'tariff missing a cost for a step is considered not initialised'() {
        given:
        SteppedTariff tariff = new SteppedTariff('test')
        tariff.addStep(new BigDecimal('0.049'), null, new BigDecimal('14'))
        tariff.addStep(null, new BigDecimal('14'), null)

        when:
        Boolean result = tariff.isInitialised()

        then:
        result == false
    }

    def 'calculate correctly calculates cost'() {
        given:
        SteppedTariff tariff = new SteppedTariff('test', steps)
        tariff.addKwh(new BigDecimal(kwh))

        when:
        BigDecimal cost = tariff.calculate(days)

        then:
        cost.setScale(2, RoundingMode.HALF_UP) == new BigDecimal(expected)

        where:
        steps                                                                                    | kwh    | days | expected
        [bdStpMp('0.15', null, '14'), bdStpMp('0.049', '14', null)]                              | '23.5' | 3    | '3.53'
        [bdStpMp('0.15', null, '14'), bdStpMp('0.049', '14', null)]                              | '23.5' | 1    | '2.57'
        [bdStpMp('0.15', null, '14'), bdStpMp('0.049', '14', '20'), bdStpMp('0.01', '20', null)] | '23.5' | 3    | '3.53'
        [bdStpMp('0.15', null, '14'), bdStpMp('0.049', '14', '20'), bdStpMp('0.01', '20', null)] | '23.5' | 1    | '2.43'
        [bdStpMp('0.15', null, '14'), bdStpMp('0.049', '14', '20'), bdStpMp('0.01', '20', null)] | '23.5' | 30   | '3.53'
        [bdStpMp('0.15', null, '14')]                                                            | '23.5' | 1    | '2.10'
        [bdStpMp('0.15', null, '14')]                                                            | '23.5' | 3    | '3.53'

    }

    Map<String, BigDecimal> bdStpMp(String cost, String min, String max) {
        Map<String, BigDecimal> step = [:]
        if (cost) { step.costPerKwh = new BigDecimal(cost) }
        if (min) { step.minKwh = new BigDecimal(min) }
        if (max) { step.maxKwh = new BigDecimal(max) }
        return step
    }
}
