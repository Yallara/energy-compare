/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import spock.lang.Specification


class StatsMakerSpec extends Specification {

    DataManager dataManager
    StatsMaker statsMaker

    def setup() {
        dataManager = Stub()
        statsMaker = new StatsMaker(dataManager)
    }

    def 'StatsMaker calculates correct results'() {
        given:
        List<DailyData> dataList = [
          makeData('2024-01-02', '4.567', '3.567'),
          makeData('2024-01-03', '3.45', '2.13'),
          makeData('2024-01-04', '5.432', '3.456')
        ]
        dataManager.getMeterName() >> 'testmeter'
        dataManager.calcDailyData() >> dataList
        StringWriter stringout = new StringWriter(1024)
        PrintWriter output = new PrintWriter(stringout)

        when:
        statsMaker.calculate(output)
        String result = stringout.toString()

        then:
        result.contains('Meter testmeter')
        result.contains('between 2024-01-02 and 2024-01-04')
        result.contains('imported: 13.4 kWh')
        result.contains('exported: 9.2 kWh')
        result.contains('import: 4.5 kWh')
        result.contains('export: 3.1 kWh')

    }

    DailyData makeData(String date, String imported, String exported) {
        DailyData data = new DailyData()
        data.setUseDate(date)
        data.setImported(imported)
        data.setExported(exported)
        return data
    }

}
