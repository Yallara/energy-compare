/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import au.yallara.energy.exception.DbException
import au.yallara.energy.tariff.SimpleTariff
import au.yallara.energy.tariff.Tariff
import au.yallara.energy.tariff.TouTariff
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalTime


class PlanComparatorSpec extends Specification {

    DataManager dataManager
    PlanComparator testObj

    def setup() {
        dataManager = Stub()
        TimezoneSupport tzs = new TimezoneSupport(null, null)
        testObj = new PlanComparator(dataManager, tzs)
    }


    def 'plan config loads correctly for simple plan'() {
        given:
        ConfigObject conf = new ConfigSlurper().parse(simpleConfig)

        when:
        List<EnergyPlanDetails> result = testObj.energyPlanConfToData(conf)
        EnergyPlanDetails firstPlan = result.first()
        SimpleTariff firstImportTariff = firstPlan.importTariffs.first() as SimpleTariff

        then:
        result.size() == 1
        firstPlan.planName == 'testPlan'
        firstPlan.supplyDailyCost == new BigDecimal('1.2134')
        firstPlan.exportTariffs.isEmpty()
        firstPlan.importTariffs.size() == 1
        firstImportTariff.costPerKwh == new BigDecimal('0.3245')
        firstImportTariff.tariffName == 'only Tariff'
    }

    def 'plan config loads correctly for complex plan'() {
        given:
        ConfigObject conf = new ConfigSlurper().parse(complexConfig)

        when:
        List<EnergyPlanDetails> result = testObj.energyPlanConfToData(conf)
        EnergyPlanDetails firstPlan = result.find { it.planName == 'firstPlan' }
        EnergyPlanDetails secondPlan = result.find { it.planName == 'secondPlan' }
        EnergyPlanDetails thirdPlan = result.find { it.planName == 'thirdPlan' }

        then:
        result.size() == 3
        firstPlan != null
        firstPlan.supplyDailyCost == new BigDecimal('1.234')
        firstPlan.importTariffs.size() == 4
        firstPlan.importTariffs*.tariffName ==~ ['peak', 'offpeak1', 'offpeak2', 'shoulder']
        firstPlan.exportTariffs.size() == 1
        secondPlan.supplyDailyCost == new BigDecimal('1.2134')
        secondPlan.importTariffs.size() == 1
        (secondPlan.importTariffs.first() as SimpleTariff).costPerKwh == new BigDecimal('0.3245')
        secondPlan.exportTariffs.size() == 1
        (secondPlan.exportTariffs.first() as SimpleTariff).costPerKwh == new BigDecimal('0.056')
        thirdPlan.supplyDailyCost == new BigDecimal('0.732')
        thirdPlan.importTariffs.size() == 2
        thirdPlan.importTariffs.collect { (it as TouTariff).costPerKwh } ==~ [new BigDecimal('0.4732'), new BigDecimal('0.211')]
        thirdPlan.exportTariffs.isEmpty()
    }

    def 'start date correctly set'() {
        given:
        dataManager.getFirstDbDate() >> dbStartDate
        dataManager.getLastDbDate() >> LocalDate.of(2024, 5, 5)

        when:
        LocalDate result = testObj.makeStartDate(origStart, origEnd)

        then:
        result == expected

        where:
        dbStartDate               | origStart                 | origEnd                  | expected
        LocalDate.of(2023, 10, 1) | LocalDate.of(2023, 11, 2) | LocalDate.of(2024, 4, 2) | LocalDate.of(2023, 11, 2)
        LocalDate.of(2023, 1, 2)  | null                      | LocalDate.of(2024, 3, 2) | LocalDate.of(2023, 3, 2)
        LocalDate.of(2024, 3, 2)  | null                      | LocalDate.of(2024, 5, 3) | LocalDate.of(2024, 3, 2)
        LocalDate.of(2024, 3, 2)  | LocalDate.of(2024, 2, 2)  | LocalDate.of(2024, 5, 4) | LocalDate.of(2024, 3, 2)
    }

    def 'end date correctly set'() {
        given:
        dataManager.getLastDbDate() >> dbEndDate
        dataManager.getFirstDbDate() >> LocalDate.of(2023, 5, 5)

        when:
        LocalDate result = testObj.makeEndDate(origEnd)

        then:
        result == expected

        where:
        dbEndDate                | origEnd                  | expected
        LocalDate.of(2024, 3, 2) | LocalDate.of(2024, 2, 2) | LocalDate.of(2024, 2, 2)
        LocalDate.of(2024, 3, 2) | null                     | LocalDate.of(2024, 3, 2)
        LocalDate.of(2024, 3, 2) | LocalDate.of(2024, 4, 4) | LocalDate.of(2024, 3, 2)
    }

    def 'if no last date in db, makeEndDate throws exception'() {
        given:
        dataManager.getLastDbDate() >> null

        when:
        LocalDate result = testObj.makeEndDate(null)

        then:
        thrown(DbException)
    }

    def 'actual dates set correctly'() {
        given:
        dataManager.getLastDbDate() >> lastDate
        dataManager.getFirstDbDate() >> firstDate

        when:
        testObj.setActualDates(origStart, origEnd)

        then:
        testObj.actualStart == realStart
        testObj.actualEnd == realEnd

        where:
        firstDate        | lastDate       | origStart            | origEnd            | realStart                       | realEnd
        dta.dbLongStart  | dta.dbLongEnd  | null                 | null               | dta.longYrBeforeEnd             | dta.dbLongEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longBeforeStart  | dta.longAfterEnd   | dta.dbLongStart                 | dta.dbLongEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longAfterStart   | dta.longAfterEnd   | dta.longAfterStart              | dta.dbLongEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longBeforeStart  | dta.longBeforeEnd  | dta.dbLongStart                 | dta.longBeforeEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longAfterStart   | dta.longBeforeEnd  | dta.longAfterStart              | dta.longBeforeEnd
        dta.dbLongStart  | dta.dbLongEnd  | null                 | dta.longBeforeEnd  | dta.longBeforeEnd.minusYears(1) | dta.longBeforeEnd
        dta.dbLongStart  | dta.dbLongEnd  | null                 | dta.longAfterEnd   | dta.longYrBeforeEnd             | dta.dbLongEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longBeforeStart  | null               | dta.dbLongStart                 | dta.dbLongEnd
        dta.dbLongStart  | dta.dbLongEnd  | dta.longAfterStart   | null               | dta.longAfterStart              | dta.dbLongEnd
        dta.dbShortStart | dta.dbShortEnd | null                 | null               | dta.dbShortStart                | dta.dbShortEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortBeforeStart | dta.shortAfterEnd  | dta.dbShortStart                | dta.dbShortEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortAfterStart  | dta.shortAfterEnd  | dta.shortAfterStart             | dta.dbShortEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortBeforeStart | dta.shortBeforeEnd | dta.dbShortStart                | dta.shortBeforeEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortAfterStart  | dta.shortBeforeEnd | dta.shortAfterStart             | dta.shortBeforeEnd
        dta.dbShortStart | dta.dbShortEnd | null                 | dta.shortBeforeEnd | dta.dbShortStart                | dta.shortBeforeEnd
        dta.dbShortStart | dta.dbShortEnd | null                 | dta.shortAfterEnd  | dta.dbShortStart                | dta.dbShortEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortBeforeStart | null               | dta.dbShortStart                | dta.dbShortEnd
        dta.dbShortStart | dta.dbShortEnd | dta.shortAfterStart  | null               | dta.shortAfterStart             | dta.dbShortEnd

    }

    def 'actual dates throw exception when passed dates out of range'() {
        given:
        String exceptionMsg = " from ${dta.dbShortStart} to ${dta.dbShortEnd}.".toString()
        dataManager.getLastDbDate() >> lastDate
        dataManager.getFirstDbDate() >> firstDate

        when:
        testObj.setActualDates(origStart, origEnd)

        then:
        DbException dbe = thrown()
        dbe.message.contains(exceptionMsg)

        where:
        firstDate        | lastDate       | origStart      | origEnd
        dta.dbShortStart | dta.dbShortEnd | null           | dta.beforeDbStart
        dta.dbShortStart | dta.dbShortEnd | dta.afterDbEnd | null
    }

    def 'invalid plans detected'() {
        when:
        List<String> result = testObj.checkPlansValid(buildDodgyPlans())

        then:
        [
          'No supply cost for plan noSupply',
          'No import tariffs for plan noTariffs',
          'No tariff found for month any, day any, time 17:00 in plan missingTariff',
          'No tariff found for month any, day any, time 17:30 in plan missingTariff',
          'No tariff found for month any, day any, time 18:00 in plan missingTariff',
          'Too many tariffs (2) apply at month any, day any, time 11:00 for plan extraTariff',
          'Too many tariffs (2) apply at month any, day any, time 22:30 for plan extraTariff',
          'Tariff dodgy in plan noTimes is missing information'
        ].each {
            result.contains(it)
        }
    }

    List<EnergyPlanDetails> buildDodgyPlans() {
        return [
          new EnergyPlanDetails(planName: 'noSupply', importTariffs:
            [
              new SimpleTariff('single', new BigDecimal('0.23'))
            ]
          ),
          new EnergyPlanDetails(planName: 'noTariffs', supplyDailyCost: new BigDecimal('1.23')),
          new EnergyPlanDetails(planName: 'missingTariff', supplyDailyCost: new BigDecimal('1.23'),
                                importTariffs: [
                                  new TouTariff('early')
                                    .setTimeRange(LocalTime.of(0, 0), LocalTime.of(15, 0))
                                    .setCostPerKwh(new BigDecimal('0.23')),
                                  new TouTariff('mid')
                                    .setTimeRange(LocalTime.of(15, 0), LocalTime.of(17, 0))
                                    .setCostPerKwh(new BigDecimal('0.341')),
                                  new TouTariff('late')
                                    .setTimeRange(LocalTime.of(18, 30), LocalTime.of(23, 59))
                                    .setCostPerKwh(new BigDecimal('0.345'))
                                ]
          ),
          new EnergyPlanDetails(planName: 'extraTariff', supplyDailyCost: new BigDecimal('1.23'),
                                importTariffs: [
                                  new TouTariff('first')
                                    .setTimeRange(LocalTime.of(0, 0), LocalTime.of(11, 30))
                                    .setCostPerKwh(new BigDecimal('0.13')),
                                  new TouTariff('second')
                                    .setTimeRange(LocalTime.of(11, 0), LocalTime.of(23, 0))
                                    .setCostPerKwh(new BigDecimal('0.34')),
                                  new TouTariff('third')
                                    .setTimeRange(LocalTime.of(22, 30), LocalTime.of(23, 59))
                                    .setCostPerKwh(new BigDecimal('0.234'))
                                ]
          ),
          new EnergyPlanDetails(planName: 'noTimes', supplyDailyCost: new BigDecimal('0.98'),
                                importTariffs: [
                                  new TouTariff('early')
                                    .setTimeRange(LocalTime.of(0, 0), LocalTime.of(15, 0))
                                    .setCostPerKwh(new BigDecimal('0.23')),
                                  new TouTariff('dodgy').setCostPerKwh(new BigDecimal('0.345')),
                                  new TouTariff('remainder').setCostPerKwh(new BigDecimal('0.234'))
                                                            .setTimeRange(LocalTime.of(15, 0), LocalTime.of(23, 59))
                                ])
        ]
    }


    static final String simpleConfig = '''
testPlan {
  supplyCost = 1.2134
  importTariffs = [
    [
      name: 'only Tariff',
      tariff: 0.3245
    ]
  ]
}
'''

    static final String complexConfig = '''
firstPlan {
  supplyCost = 1.234
  importTariffs = [
    [
      name: 'peak',
      startTime: '15:00',
      endTime: '21:00',
      tariff: 0.432
    ],
    [
      name: 'offpeak1',
      startTime: '21:00',
      endTime: '23:59',
      tariff: 0.2134
    ],
    [
      name: 'offpeak2',
      startTime: '00:00',
      endTime: '06:00',
      tariff: 0.2134
    ],
    [
      name: 'shoulder',
      startTime: '06:00',
      endTime: '15:00',
      tariff: 0.2567
    ]
  ]
  exportTariffs = [
    [
      max: 14,
      tariff: 0.12
    ],
    [
      min:14,
      tariff: 0.049
    ]
  ]
}
secondPlan {
  supplyCost = 1.2134
  importTariffs = [
    [
      name: 'only Tariff',
      tariff: 0.3245
    ]
  ]
  exportTariffs = [
    [
      tariff: 0.056
    ]
  ]
}
thirdPlan {
  supplyCost = 0.732
  importTariffs = [
    [
      name: 'Peak',
      startTime: '16:00',
      endTime: '23:59',
      tariff: 0.4732
    ],
    [
      name: 'Off Peak',
      startTime: '00:00',
      endTime: '16:00',
      tariff: 0.211
    ]
  ]
}
'''

    static final Map<String, LocalDate> dta = [
      dbShortStart    : LocalDate.of(2023, 11, 5),
      dbShortEnd      : LocalDate.of(2024, 4, 3),
      dbLongStart     : LocalDate.of(2023, 1, 2),
      dbLongEnd       : LocalDate.of(2024, 4, 3),
      longBeforeStart : LocalDate.of(2022, 11, 12),
      longBeforeEnd   : LocalDate.of(2024, 3, 2),
      longAfterStart  : LocalDate.of(2023, 5, 5),
      longAfterEnd    : LocalDate.of(2024, 5, 5),
      shortBeforeStart: LocalDate.of(2023, 2, 3),
      shortAfterStart : LocalDate.of(2023, 12, 3),
      shortBeforeEnd  : LocalDate.of(2024, 3, 3),
      shortAfterEnd   : LocalDate.of(2024, 5, 5),
      longYrBeforeEnd : LocalDate.of(2023, 4, 3),
      beforeDbStart   : LocalDate.of(2023, 11, 1),
      afterDbEnd      : LocalDate.of(2024, 4, 5),
    ]


}
