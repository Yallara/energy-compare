/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.energy

import au.yallara.energy.tariff.SteppedTariff
import au.yallara.energy.tariff.Tariff
import au.yallara.energy.tariff.TouTariff
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalTime


class CostBreakdownSpec extends Specification {

    def 'supply charge calculation correct'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()

        when:
        breakdown.days = 30 // will be 1.234 * 30

        then:
        breakdown.getSupplyCharge().setScale(Util.currencyScale, Util.round) == new BigDecimal('37.02')
    }

    def 'addKwh() works correctly for total export'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()
        breakdown.days = 1

        when:
        breakdown.addKwh(direction, aDay, start, new BigDecimal(kwh))
        BigDecimal energyOut = breakdown.getExportKwh()

        then:
        energyOut == new BigDecimal(expectedExport)

        where:
        direction           | aDay                     | start                | kwh  | expectedExport
        EnergyDirection.OUT | LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '12.43'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '0.0'
    }

    def 'addKwh() works correctly for total import'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()
        breakdown.days = 1

        when:
        breakdown.addKwh(direction, aDay, start, new BigDecimal(kwh))
        BigDecimal energyIn = breakdown.getImportKwh()

        then:
        energyIn == new BigDecimal(expectedImport)

        where:
        direction           | aDay                     | start                | kwh  | expectedImport
        EnergyDirection.OUT | LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '0.0'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '12.43'
    }

    def 'addKwh() works correctly for peak import'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()
        breakdown.days = 1

        when:
        breakdown.addKwh(direction, aDay, start, new BigDecimal(kwh))
        BigDecimal energyIn = breakdown.getTariffKwh('peak')

        then:
        energyIn == new BigDecimal(expectedImport)

        where:
        direction           | aDay                     | start                | kwh  | expectedImport
        EnergyDirection.OUT | LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '0.0'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(16, 30) | '12.43' | '12.43'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '0.0'
    }

    def 'addKwh() works correctly for offPeak import'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()
        breakdown.days = 1

        when:
        breakdown.addKwh(direction, aDay, start, new BigDecimal(kwh))
        BigDecimal energyIn = breakdown.getTariffKwh('offPeak')

        then:
        energyIn == new BigDecimal(expectedImport)

        where:
        direction           | aDay                     | start                | kwh  | expectedImport
        EnergyDirection.OUT | LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '0.0'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(16, 30) | '12.43' | '0.0'
        EnergyDirection.IN |  LocalDate.of(2024, 4, 4) | LocalTime.of(11, 30) | '12.43' | '12.43'
    }

    def 'calculate total cost correctly'() {
        given:
        CostBreakdown breakdown = buildBasicTestObj()
        LocalDate dt = LocalDate.of(2024,4,4)
        breakdown.addKwh(EnergyDirection.OUT, dt, LocalTime.of(11,30), new BigDecimal(export))
        breakdown.addKwh(EnergyDirection.IN, dt, LocalTime.of(11,30), new BigDecimal(offpeak))
        breakdown.addKwh(EnergyDirection.IN, dt, LocalTime.of(18,0), new BigDecimal(peak))
        breakdown.days = days

        when:
        BigDecimal result = breakdown.total.setScale(3, Util.round)

        then:
        result == new BigDecimal(expected)

        where:
       export   | peak     | offpeak  | days | expected
        '3.54'   | '1.23'   | '5.321'  | 2    | '3.870'
        '92.432' | '26.789' | '35.654' | 30   | '46.017'
        '92.432' | '26.789' | '35.654' | 3    | '16.279'
        '0.0'    | '26.789' | '35.654' | 3    | '23.790'
        '92.432' | '0.0'    | '35.654' | 3    | '4.947'
        '92.432' | '26.789' | '0.0'    | 3    | '7.523'
    }

    CostBreakdown buildBasicTestObj() {
        EnergyPlanDetails plan = buildPlan()
        return new CostBreakdown(plan, LocalDate.of(2023, 3, 3), LocalDate.of(2024, 3, 3))
    }

    EnergyPlanDetails buildPlan() {

        List<Tariff> importTariffs = [
          new TouTariff('peak').setTimeRange(LocalTime.of(15, 0), LocalTime.of(23, 59)).setCostPerKwh(new BigDecimal('0.423')),
          new TouTariff('offPeak').setTimeRange(LocalTime.of(0, 0), LocalTime.of(15, 0)).setCostPerKwh(new BigDecimal('0.2456'))
        ]
        List<Tariff> exportTariffs = [
          new SteppedTariff('export', [
            [costPerKwh: new BigDecimal('0.12'), minKwh: null, maxKwh: new BigDecimal('14')],
            [costPerKwh: new BigDecimal('0.049'), minKwh: new BigDecimal('14'), maxKwh: null ]
          ]),
        ]

        return new EnergyPlanDetails('testing', new BigDecimal('1.234'),
                                     exportTariffs, importTariffs)
    }
}
