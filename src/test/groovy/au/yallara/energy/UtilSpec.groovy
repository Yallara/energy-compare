/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import spock.lang.Specification

import java.sql.Time
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.LocalDate
import java.time.Month


class UtilSpec extends Specification {

    def 'asBigDecimal() copes with valid input'() {
        when:
        def result = Util.asBigDecimal(data)

        then:
        result instanceof BigDecimal
        result == expected

        where:
        data                   | expected
        new BigDecimal('10.5') | new BigDecimal('10.5')
        5                      | new BigDecimal('5')
        '4.56'                 | new BigDecimal('4.56')
        3.45                   | new BigDecimal('3.45')
        null                   | BigDecimal.ZERO
        ''                     | BigDecimal.ZERO
    }

    def 'asBigDecimal() throws exception when given invalid data'() {
        when:
        def result = Util.asBigDecimal(data)

        then:
        thrown NumberFormatException

        where:
        data            | expected
        'hello'         | '-'
        '123cde'        | '-'
        LocalDate.now() | '-'
    }

    def 'asLocalTime() copes with valid input'() {
        when:
        def result = Util.asLocalTime(data)

        then:
        result instanceof LocalTime
        result == expected

        where:
        data                                  | expected
        LocalTime.of(1, 2, 3)                 | LocalTime.of(1, 2, 3)
        LocalDateTime.of(2024, 1, 2, 3, 4, 5) | LocalTime.of(3, 4, 5)
        Time.valueOf('15:04:06')              | LocalTime.of(15, 4, 6)
        '13:09:56'                            | LocalTime.of(13, 9, 56)
        '14:45'                               | LocalTime.of(14, 45)
    }

    def 'asLocalTime() throws exception when passed invalid input'() {
        when:
        def result = Util.asLocalTime(data)

        then:
        thrown NumberFormatException

        where:
        data    | expected
        null    | '-'
        'hello' | '-'
        ''      | '-'
        0       | '-'
        45      | '-'
    }

    def 'asCurrencyString formats correctly'() {
        when:
        def result = Util.asCurrencyString(data)

        then:
        result == expected

        where:
        data                      | expected
        new BigDecimal('5.03')    | '$5.03'
        new BigDecimal('1432.32') | '$1,432.32'
        new BigDecimal('17.886')  | '$17.89'
        new BigDecimal('17.832')  | '$17.83'
        null                      | ''
    }

    def 'asKwhString() formats correctly'() {
        when:
        def result = Util.asKwhString(data)

        then:
        result == expected

        where:
        data                       | expected
        new BigDecimal('4321.567') | '4,321.6'
        new BigDecimal('34.7222')  | '34.7'
        null                       | ''
    }

    def 'asDaysOfWeek() converts correctly'() {
        when:
        def result = Util.asDaysOfWeek(data)

        then:
        result ==~ expected

        where:
        data                        | expected
        'MONDAY'                    | [DayOfWeek.MONDAY]
        'Monday,WEDNESDAY'          | [DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY]
        'Monday,FRIDAY,MONDAY'      | [DayOfWeek.MONDAY, DayOfWeek.FRIDAY]
        'MONDAY, TUESDAY,   FRIDAY' | [DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.FRIDAY]
    }

    def 'asMonths() converts correctly'() {
        when:
        def result = Util.asMonths(data)

        then:
        result ==~ expected

        where:
        data                  | expected
        'APRIL'               | [Month.APRIL]
        'April,AUGUST'        | [Month.APRIL, Month.AUGUST]
        'May,April,May,APril' | [Month.APRIL, Month.MAY]
        'May,  APril'         | [Month.APRIL, Month.MAY]
    }


}
