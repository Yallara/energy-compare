/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import spock.lang.Specification
import spock.lang.Unroll


class DataManagerSpec extends Specification {

    def 'toDbDate() converts correctly'() {
        given:
        DataManager testObj = new DataManager('dbpath', 'name')

        when:
        def result = testObj.toDbDate('20240405')

        then:
        result == '2024-04-05'
    }

    @Unroll
    def 'toDbDate() throws expection when invalid data'() {
        given:
        DataManager testObj = new DataManager('dbpath', 'name')

        when:
        def result = testObj.toDbDate(data)

        then:
        thrown IllegalArgumentException

        where:
        data       | expected
        null       | ''
        ''         | ''
        '202403'   | ''
        '        ' | ''
    }

}
