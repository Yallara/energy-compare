/*
 * Copyright (c) 2024. Yallara
 * This file is part of EnergyCompare.
 *
 * EnergyCompare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnergyCompare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnergyCompare.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.energy

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime


class TimezoneSupportSpec extends Specification {

    def 'timezone zone is correct'() {
        given:
        TimezoneSupport testObj = new TimezoneSupport(offset, zoneName)

        when:
        def result = testObj.getZoneZoneId()

        then:
        result.id == zoneName

        where:
        offset      | zoneName
        'UTC+10:00' | 'Australia/Melbourne'
        'UTC+10:00' | 'Australia/Brisbane'
        'UTC+08:00' | 'Australia/Perth'
    }

    def 'offset zone is correct'() {
        given:
        TimezoneSupport testObj = new TimezoneSupport(offset, zoneName)

        when:
        def result = testObj.getOffsetZoneId()

        then:
        result.id == offset

        where:
        offset      | zoneName
        'UTC+10:00' | 'Australia/Melbourne'
        'UTC+10:00' | 'Australia/Brisbane'
        'UTC+08:00' | 'Australia/Perth'
    }

    def 'isDstTransition() is correct'() {
        given:
        TimezoneSupport testObj = new TimezoneSupport(offset, zoneName)
        LocalDate dt = LocalDate.of(yr, mn, dy)

        when:
        def result = testObj.isDstTransition(dt)

        then:
        result == expected

        where:
        offset      | zoneName              | yr   | mn | dy | expected
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | true
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | true
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 8  | false
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | false
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | false
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 8  | false
    }

    def 'offset converted to zone datetime'() {
        given:
        TimezoneSupport testObj = new TimezoneSupport(offset, zone)
        LocalDateTime from = LocalDateTime.of(yr, mn, dy, hr, min)

        when:
        LocalDateTime result = testObj.offsetToZone(from)

        then:
        result == LocalDateTime.of(ryr, rmn, rdy, rhr, rmin)

        where:
        offset      | zone                  | yr   | mn | dy | hr | min | ryr  | rmn | rdy | rhr | rmin
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 1  | 0   | 2024 | 4   | 7   | 2   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 1  | 30  | 2024 | 4   | 7   | 2   | 30
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 2  | 0   | 2024 | 4   | 7   | 2   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 3  | 0   | 2024 | 4   | 7   | 3   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 1  | 0   | 2023 | 10  | 1   | 1   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 1  | 30  | 2023 | 10  | 1   | 1   | 30
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 2  | 0   | 2023 | 10  | 1   | 3   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 3  | 0   | 2023 | 10  | 1   | 4   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 1  | 0   | 2024 | 4   | 7   | 1   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 1  | 30  | 2024 | 4   | 7   | 1   | 30
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 2  | 0   | 2024 | 4   | 7   | 2   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 3  | 0   | 2024 | 4   | 7   | 3   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 1  | 0   | 2023 | 10  | 1   | 1   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 1  | 30  | 2023 | 10  | 1   | 1   | 30
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 2  | 0   | 2023 | 10  | 1   | 2   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 3  | 0   | 2023 | 10  | 1   | 3   | 0
    }

    def 'zone converted to offset datetime'() {
        given:
        TimezoneSupport testObj = new TimezoneSupport(offset, zone)
        LocalDateTime from = LocalDateTime.of(yr, mn, dy, hr, min)

        when:
        LocalDateTime result = testObj.zoneToOffset(from)

        then:
        result == LocalDateTime.of(ryr, rmn, rdy, rhr, rmin)

        where:
        offset      | zone                  | yr   | mn | dy | hr | min | ryr  | rmn | rdy | rhr | rmin
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 1  | 0   | 2024 | 4   | 7   | 0   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 1  | 30  | 2024 | 4   | 7   | 0   | 30
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 2  | 0   | 2024 | 4   | 7   | 1   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2024 | 4  | 7  | 3  | 0   | 2024 | 4   | 7   | 3   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 1  | 0   | 2023 | 10  | 1   | 1   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 1  | 30  | 2023 | 10  | 1   | 1   | 30
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 3  | 0   | 2023 | 10  | 1   | 2   | 0
        'UTC+10:00' | 'Australia/Melbourne' | 2023 | 10 | 1  | 4  | 0   | 2023 | 10  | 1   | 3   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 1  | 0   | 2024 | 4   | 7   | 1   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 1  | 30  | 2024 | 4   | 7   | 1   | 30
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 2  | 0   | 2024 | 4   | 7   | 2   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2024 | 4  | 7  | 3  | 0   | 2024 | 4   | 7   | 3   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 1  | 0   | 2023 | 10  | 1   | 1   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 1  | 30  | 2023 | 10  | 1   | 1   | 30
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 2  | 0   | 2023 | 10  | 1   | 2   | 0
        'UTC+10:00' | 'Australia/Brisbane'  | 2023 | 10 | 1  | 3  | 0   | 2023 | 10  | 1   | 3   | 0
    }

}
